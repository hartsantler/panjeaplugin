#!/bin/bash
cd
sudo add-apt-repository universe

## this workaround for pulseeffects and libboost on Ubuntu20.04 is broken, Ubuntu20.10 is now required.
#sudo add-apt-repository ppa:mikhailnov/pulseeffects

sudo apt-get update
sudo apt-get install curl git cmake build-essential python3-dev python3-pip
sudo apt-get install python2 python-ply python-pycparser python-numpy libevent-dev zip espeak vorbis-tools uglifyjs p7zip-full python3-pyqt5 python3-pyqt5.qtwebengine python3-opencv python3-pyaudio python3-numpy python3-scipy python3-pygame python3-nose python3-pil ffmpeg ttf-ancient-fonts libgtk-3-dev libboost-all-dev v4l-utils libv4l-dev xterm cython3 libpotrace-dev pavucontrol qtractor jack-tools hydrogen guitarix

cd
git clone https://github.com/atareao/python3-v4l2capture
cd python3-v4l2capture/
python3 setup.py build
sudo python3 setup.py install

cd
git clone https://gitlab.com/hartsantler/dlib.git
cd dlib
python3 setup.py build
sudo python3 setup.py install


## https://github.com/mixxxdj/mixxx/wiki/Compiling%20on%20Linux
cd
sudo apt-get install g++ git scons cmake ccache libportmidi-dev libopusfile-dev \
  libshout-dev libtag1-dev libprotobuf-dev protobuf-compiler \
  libusb-1.0-0-dev libfftw3-dev libmad0-dev portaudio19-dev \
  libchromaprint-dev librubberband-dev libsqlite3-dev \
  libid3tag0-dev libflac-dev libsndfile-dev libupower-glib-dev \
  libavcodec-dev libavformat-dev libavutil-dev libswresample-dev \
  libgl-dev liblilv-dev libjack-dev libjack0 libmp3lame-dev libebur128-dev qt5keychain-dev libmodplug-dev

sudo apt-get install qt5-default qt5keychain-dev qtdeclarative5-dev libqt5opengl5-dev qtscript5-dev libqt5svg5-dev libqt5x11extras5-dev libvamp-sdk2v5 libhidapi-libusb0 libqt5sql5-sqlite libmodplug-dev libsoundtouch-dev
## TODO
#cd panmixxx
#scons faad=0 ffmpeg=1


## note: python3-pyo removes portaudio19-dev because it conflicts with jack2?
#cd
#sudo apt-get install python3-wxgtk4.0 python3-pyo
#git clone https://gitlab.com/hartsantler/panjia.git

sudo apt-get install devscripts equivs

cd
sudo apt-get install mplayer qtbase5-dev qtbase5-dev-tools qttools5-dev qttools5-dev-tools qtwebengine5-dev libqt5xmlpatterns5-dev libarchive-dev libsndfile1-dev libasound2-dev liblo-dev libpulse-dev libcppunit-dev liblrdf-dev librubberband-dev libtar-dev
git clone https://gitlab.com/hartsantler/pandrogen.git
cd pandrogen
mkdir build
cd build
cmake ..
make
## installing to /usr/local makes this bug 
## /usr/local/bin/pandrogen: error while loading shared libraries: libhydrogen-core-1.1.0.so: cannot open shared object file: No such file or directory
sudo make install
## workaround is to run from the build folder: ~/pandrogen/build/src/gui/pandrogen

cd
git clone https://gitlab.com/hartsantler/pypotrace.git
cd pypotrace
python3 setup.py build
sudo python3 setup.py install

#sudo apt-get remove libboost1.71-dev libboost-system1.71-dev libboost-filesystem1.71-dev
cd
git clone https://gitlab.com/hartsantler/pulseeffects.git
cd pulseeffects/
sudo mk-build-deps --install debian/control
debuild -i -us -uc
sudo apt install ../*pulseeffects*.deb

sudo apt install python3-pip
pip3 install torch==1.7.1+cpu torchvision==0.8.2+cpu torchaudio==0.7.2 -f https://download.pytorch.org/whl/torch_stable.html
git clone https://github.com/zongyi-li/fourier_neural_operator

sudo apt-get install libgtk2.0-dev liboscpack-dev libsdl2-dev
git clone https://gitlab.com/hartsantler/amsynth.git
cd amsynth
mkdir build
cmake ../.
make
sudo make install

