#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Panjea Plugin
# by: hartsantler
# copyright: panjea.com
# license: GNU GPL

FORCE_OFFLINE = False
DEPRECATED = False

import os, sys, platform, subprocess, webbrowser, random, stat, shutil

OSTYPE = platform.system()

def log(msg, msg2=None):
	try:
		sys.stderr.write( 'MSG: ' + str(msg) + '\n' )
		if msg2 is not None:
			sys.stderr.write( '	' + str(msg2) + '\n' )
		sys.stderr.flush()
	except:
		pass


log('OS TYPE:', OSTYPE)
USERNAME = None
DESKTOP_TYPE = None
if OSTYPE=='Linux':
	if 'XDG_CURRENT_DESKTOP' in os.environ:
		DESKTOP_TYPE = os.environ['XDG_CURRENT_DESKTOP']
	log("DESKTOP TYPE:", DESKTOP_TYPE)
	## https://stackoverflow.com/questions/47444178/difference-between-os-getlogin-and-os-environ-for-getting-username
	try:
		USERNAME = os.getlogin()
	except:
		USERNAME = os.environ['USER']
	log("USER NAME:", USERNAME)

BLENDER27 = '--blender27' in sys.argv
BLENDER28 = '--blender28' in sys.argv
THISDIR = os.path.split( os.path.abspath(__file__) )[0]
log('THISDIR:', THISDIR)

if sys.version_info.major==3:
	unicode = sys
	long = int
	ISPY3 = True
else:
	ISPY3 = False
	log("WARN: Python2 is deprecated")

ISPLUGIN = False
ISSUBPROC = False
if '--qt' in sys.argv or '--tk' in sys.argv or '--pipe' in sys.argv:
	ISSUBPROC = True

try:
	import bpy
	ISPLUGIN = True
	if bpy.app.version[1] < 80:
		BLENDER27 = True
except:
	BLENDER_PATH = None
	for arg in sys.argv:
		if arg.startswith('--blender='):
			bpath = arg.split('=')[-1]
			if os.path.isfile(bpath):
				BLENDER_PATH = bpath
			else:
				log("ERROR: invalid path to blender")
				sys.exit(1)

	if BLENDER_PATH is None:
		if OSTYPE=='Darwin':
			# https://docs.blender.org/manual/en/latest/advanced/command_line/launch/macos.html
			if os.path.isfile('/Applications/Blender.app/Contents/MacOS/Blender'):
				BLENDER_PATH = '/Applications/Blender.app/Contents/MacOS/Blender'
			else:
				webbrowser.open( 'https://www.blender.org/download/' )
				log('you must install blender first')
				sys.exit(1)

		elif OSTYPE=='Linux':
			if BLENDER27:
				if not os.path.isdir( os.path.expanduser('~/blender-2.79b-linux-glibc219-x86_64')):
					subprocess.check_call(['wget', '-c', 'https://download.blender.org/release/Blender2.79/blender-2.79b-linux-glibc219-x86_64.tar.bz2'], cwd=os.path.expanduser('~/'))
					subprocess.check_call(['tar', '-xvf', 'blender-2.79b-linux-glibc219-x86_64.tar.bz2'], cwd=os.path.expanduser('~/'))
				if os.path.isdir( os.path.expanduser('~/blender-2.79b-linux-glibc219-x86_64')):
					BLENDER_PATH = os.path.expanduser('~/blender-2.79b-linux-glibc219-x86_64/blender')
				elif os.path.isfile('/usr/bin/blender'):
					BLENDER_PATH = '/usr/bin/blender'
				else:
					raise RuntimeError('can not find blender2.7')

			elif os.path.isfile( os.path.expanduser('~/blender-2.91.2-linux64/blender') ) and not BLENDER28:
				BLENDER_PATH = os.path.expanduser('~/blender-2.91.2-linux64/blender')

			elif os.path.isfile( os.path.expanduser('~/blender-2.91.0-linux64/blender') ) and not BLENDER28:
				BLENDER_PATH = os.path.expanduser('~/blender-2.91.0-linux64/blender')

			elif os.path.isfile( os.path.expanduser('~/blender-2.90.1-linux64/blender') ) and not BLENDER28:
				BLENDER_PATH = os.path.expanduser('~/blender-2.90.1-linux64/blender')
			elif os.path.isfile( os.path.expanduser('~/blender-2.90.0-linux64/blender') ) and not BLENDER28:
				BLENDER_PATH = os.path.expanduser('~/blender-2.90.0-linux64/blender')
			elif os.path.isfile( os.path.expanduser('~/blender-2.83.5-linux64/blender') ):
				BLENDER_PATH = os.path.expanduser('~/blender-2.83.5-linux64/blender')
			elif os.path.isfile( os.path.expanduser('~/blender-2.83.3-linux64/blender') ):
				BLENDER_PATH = os.path.expanduser('~/blender-2.83.3-linux64/blender')
			elif os.path.isfile( os.path.expanduser('~/blender-2.83.2-linux64/blender') ):
				BLENDER_PATH = os.path.expanduser('~/blender-2.83.2-linux64/blender')
			elif os.path.isfile( os.path.expanduser('~/blender-2.83.0-linux64/blender') ):
				BLENDER_PATH = os.path.expanduser('~/blender-2.83.0-linux64/blender')
			elif os.path.isdir( os.path.expanduser('~/blender-2.79b-linux-glibc219-x86_64')):
				## fallback to blender27
				BLENDER_PATH = os.path.expanduser('~/blender-2.79b-linux-glibc219-x86_64/blender')
				BLENDER27 = True
			## fallback to system installed blender
			elif os.path.isfile('/usr/bin/blender') and '--no-system-blender' not in sys.argv:
				BLENDER_PATH = '/usr/bin/blender'

			elif '--system-blender' not in sys.argv:
				subprocess.check_call(['wget', '-c', 'https://ftp.nluug.nl/pub/graphics/blender/release/Blender2.91/blender-2.91.2-linux64.tar.xz'], cwd=os.path.expanduser('~/'))
				subprocess.check_call(['tar', '-xvf', 'blender-2.91.2-linux64.tar.xz'], cwd=os.path.expanduser('~/'))
				BLENDER_PATH = os.path.expanduser('~/blender-2.91.2-linux64/blender')
			else:
				subprocess.check_call(['sudo', 'apt-get', 'install', 'blender'])
				BLENDER_PATH = '/usr/bin/blender'


		elif OSTYPE=='Windows':
			if not os.path.isdir('C:/tmp'):
				os.mkdir('C:/tmp')

			if BLENDER27 and os.path.isfile( 'C:/Program Files/Blender Foundation/Blender/blender.exe' ):
				BLENDER_PATH = 'C:/Program Files/Blender Foundation/Blender/blender.exe'
			elif os.path.isfile(os.path.join( THISDIR, 'blender-2.91.2-windows64/blender.exe') ):
				BLENDER_PATH = os.path.join(THISDIR, 'blender-2.91.2-windows64/blender.exe')
			elif os.path.isfile(os.path.join( THISDIR, 'blender-2.90.1-windows64/blender.exe') ):
				BLENDER_PATH = os.path.join(THISDIR, 'blender-2.90.1-windows64/blender.exe')
			elif os.path.isfile(os.path.join( THISDIR, 'blender/blender.exe') ):
				BLENDER_PATH = os.path.join(THISDIR, 'blender/blender.exe')
			elif os.path.isfile( 'C:/Program Files/Blender Foundation/Blender 2.90/blender.exe' ):
				BLENDER_PATH = 'C:/Program Files/Blender Foundation/Blender 2.90/blender.exe'
			elif os.path.isfile( 'C:/Program Files/Blender Foundation/Blender/blender.exe' ):
				BLENDER_PATH = 'C:/Program Files/Blender Foundation/Blender/blender.exe'
			else:
				if BLENDER27:
					try:
						subprocess.check_call(['curl.exe', '--output', 'blender27.msi', '--url', 'https://download.blender.org/release/Blender2.79/blender-2.79b-windows64.msi'])
						subprocess.check_call(['msiexec', '/I', 'blender27.msi'])
					except:
						webbrowser.open('https://download.blender.org/release/Blender2.79/')
						log('install blender2.79b')
						sys.exit(0)

					if os.path.isfile( 'C:/Program Files/Blender Foundation/Blender/blender.exe' ):
						BLENDER_PATH = 'C:/Program Files/Blender Foundation/Blender/blender.exe'

				else:
					BLENDERZIP = os.path.join(THISDIR,'blender.zip')
					try:
						if '--system-blender' in sys.argv:
							subprocess.check_call(['curl.exe', '-C', '-' ,'--output', 'blender29.msi', '--url', 'https://ftp.nluug.nl/pub/graphics/blender/release/Blender2.91/blender-2.91.2-windows64.msi'])
							subprocess.check_call(['msiexec', '/I', 'blender29.msi'])
						else:
							subprocess.check_call(['curl.exe', '-C', '-', '--output', 'blender.zip', '--url', 'https://ftp.nluug.nl/pub/graphics/blender/release/Blender2.91/blender-2.91.2-windows64.zip'])
					except:
						webbrowser.open('https://www.blender.org/download/')
						log('install blender2.91.2')
						sys.exit(0)

					if '--system-blender' in sys.argv:
						if os.path.isfile( 'C:/Program Files/Blender Foundation/Blender 2.91/blender.exe' ):
							BLENDER_PATH = 'C:/Program Files/Blender Foundation/Blender 2.91/blender.exe'
						elif os.path.isfile( 'C:/Program Files/Blender Foundation/Blender/blender.exe' ):
							BLENDER_PATH = 'C:/Program Files/Blender Foundation/Blender/blender.exe'
					else:
						if os.path.isfile(BLENDERZIP):
							try:
								subprocess.check_call(['powershell', '-command', 'Expand-Archive -Force %s %s' %(BLENDERZIP, THISDIR)])
							except:
								log("failed to extract blender.zip")
								log("Windows10 is required, note: older versions of Windows must install PowerShell 5")
								log("if your internet connection is unstable, download of blender may have failed, restart to resume download")
								sys.exit(0)
							if os.path.isfile(os.path.join( THISDIR, 'blender-2.91.2-windows64/blender.exe') ):
								BLENDER_PATH = os.path.join(THISDIR, 'blender-2.91.2-windows64/blender.exe')
							elif os.path.isfile(os.path.join( THISDIR, 'blender/blender.exe') ):
								BLENDER_PATH = os.path.join(THISDIR, 'blender/blender.exe')
							else:
								log("could not find blender.exe")
								log("if you are using a version of blender portable other than 2.91.2")
								log("then place it in this folder and name that folder `blender`")
								sys.exit(0)


	else:
		raise RuntimeError("UNKNOWN OS TYPE")

	if BLENDER_PATH:
		log('found blender OK')
		log(BLENDER_PATH)
	else:
		log('ERROR: could not find blender')

import time, threading, math, json, base64, atexit
from random import random, uniform, shuffle, choice, randint

ALPHABET = 'abcdefghijklmnopqrstuvwxyz'.upper()

if ISPLUGIN:
	import hashlib
	import http.server, socketserver
	from http import HTTPStatus
	SimpleHTTPServer = http.server
	SocketServer = socketserver
	Qt = None
	ode = None

	##https://blender.stackexchange.com/questions/163058/how-to-minify-simple-display-with-python-script-execution
	def simple_blender_ui():
		if BLENDER27:
			bpy.context.user_preferences.view.show_splash = False
		else:
			bpy.context.preferences.view.show_splash = False
		for window in bpy.context.window_manager.windows:
			screen = window.screen
			if not BLENDER27:
				screen.show_statusbar=False
			for area in screen.areas:
				#if area.type == 'PROPERTIES':
				if area.type == 'VIEW_3D':
					override = {'window': window, 'screen': screen, 'area': area}
					bpy.ops.screen.screen_full_area(override, use_hide_panels=True)
					break

		bpy.ops.screen.userpref_show("INVOKE_DEFAULT")  ## can not set custom size :(
		#bpy.context.scene.render.resolution_x = 240
		#bpy.context.scene.render.resolution_y = 620
		#bpy.ops.render.view_show()
		# Change area type
		area = bpy.context.window_manager.windows[-1].screen.areas[0]
		area.type = "PROPERTIES"

	simple_blender_ui()

	## TODO
	#try:
	#	import pygame
	#except:
	#	pygame = None
	#	print("WARN: you will get better gamepad performance if you install pygame for python3")
	#	print("falling back to using pygame from python2 inside a subprocess")
	#	if OSTYPE=='Linux':
	#		##https://stackoverflow.com/questions/26855293/pygame-on-ubuntu-python-3
	#		print("Ubuntu 19.04 and later")
	#		print("sudo apt-get install python3-pygame")
	#		print("Ubuntu 18.10")
	#		print("sudo apt-get install python3-pip")
	#		print("pip3 install pygame")
	_EXIT_ = False
	def exit_plugin():
		global _EXIT_
		log('panjeaplugin exit...')
		_EXIT_ = True
		if _qt_proc:
			#_qt_proc.stdin.write(b'{"exit":1}\n')
			#_qt_proc.stdin.flush()
			_qt_proc.kill()
		time.sleep(1)

	if BLENDER27:
		bpy.ops.wm.addon_enable(module='rigify')
		bpy.ops.wm.addon_enable(module='add_curve_ivygen')
	else:
		bpy.ops.preferences.addon_enable(module='rigify')
		bpy.ops.preferences.addon_enable(module='add_curve_ivygen')

	## API Main Classes ##
	BlenderObjectWrapper = None
	api = None
	class PanjeaBpyBase(object):
		blender_version = eval( '%s.%s' %(bpy.app.version[0], bpy.app.version[1]))

		def set_cursor(self, location):
			if BLENDER27:
				bpy.context.scene.cursor_location = location
			else:
				bpy.context.scene.cursor.location = location


		def new_modifier(self, ob, type=None, name=None):
			if ob.type=='GPENCIL' or type.startswith('GP_'):
				if type=="SUBSURF":
					type = "SUBDIV"
				if not type.startswith('GP_'):
					type = 'GP_' + type
				if BLENDER27:
					return None
				else:
					return ob.grease_pencil_modifiers.new(name=name or type, type=type)
			elif type in ('TWIST', 'BEND', 'TAPER', 'STRETCH'):
				mod = ob.modifiers.new(name=name or type, type='SIMPLE_DEFORM')
				mod.deform_method = type
				if BLENDER27:
					pass
				else:
					mod.deform_axis = 'Z'
			elif type in ('SPHERE', 'CYLINDER', 'CUBE', 'CUBOID'):
				mod = ob.modifiers.new(name=name or type, type='CAST')
				if type == 'CUBE':
					type = 'CUBOID'
				mod.cast_type = type
			else:
				if type=="SUBDIV":
					type = "SUBSURF"
				mod = ob.modifiers.new(name=name or type, type=type)
			if name:
				mod.name = name
			return mod

		def hex2rgb(self, hex):
			if hex.startswith('#'):
				hex = hex[1:]
			r = int(hex[:2], 16) / 255.0
			g = int(hex[2:4], 16) / 255.0
			b = int(hex[4:], 16) / 255.0
			return [round(r,2),round(g,2),round(b,2)]
		def select(self, *objs):
			if BLENDER27:
				for ob in objs:
					ob.select = True
			else:
				for ob in objs:
					ob.select_set(True)
		def unselect(self, *objs):
			if BLENDER27:
				for ob in objs:
					ob.select = False
			else:
				for ob in objs:
					ob.select_set(False)
		def link(self, ob):
			if BLENDER27:
				bpy.context.scene.objects.link( ob )
			else:
				bpy.context.collection.objects.link( ob )


		def append_file(self, name):
			return self.link_file(name, link=False)

		def download_blend(self, name):
			filepath = os.path.expanduser('~/.panjea/%s.blend' %name)
			if OSTYPE=='Windows':
				subprocess.check_call(['curl.exe', '--output', filepath, '--url', 'https://panjeastatic.s3.us-east-2.amazonaws.com/blends/%s.blend' %name])
			else:
				subprocess.check_call(['curl', '--output', filepath, '--url', 'https://panjeastatic.s3.us-east-2.amazonaws.com/blends/%s.blend' %name])

		def load(self, name):
			ob = self.link_file(name, link=True)
			return self[ ob ]

		def link_file(self, name, link=True):
			filepath = os.path.expanduser('~/.panjea/%s.blend' %name)
			if not os.path.isfile(filepath):
				self.download_blend(name)

			if BLENDER27:
				print("TODO link_file for blender2.7")
				empty = self.new_prim('empty')
				empty.name = name
				return empty
			else:
				col = None
				already_linked = False
				if name in bpy.data.collections:
					print('reusing linked file: ', name)
					col = bpy.data.collections[ name ]
					already_linked = True
				else:
					print('linking new file: ', name)
					ok = False
					try:
						with bpy.data.libraries.load(filepath, link=link) as (data_from, data_to):
							for c in data_from.collections:
								if c == name:
									data_to.collections = [c]
						ok = True
					except OSError:
						self.download_blend(name)
						try:
							with bpy.data.libraries.load(filepath, link=link) as (data_from, data_to):
								for c in data_from.collections:
									if c == name:
										data_to.collections = [c]
							ok = True
						except OSError:
							print("ERROR: can not download: ", blend)
							return None


					for group in data_to.collections:
						if group is not None:
							bpy.context.scene.collection.children.link(group)
							col = group
							break

				if col:
					ob = bpy.data.objects.new(name=col.name, object_data=None)
					ob.instance_type = 'COLLECTION'
					ob.instance_collection = col
					self.link(ob)

					if not already_linked:
						for sub in col.objects:
							self.parse_modifiers(sub)

						bpy.context.view_layer.layer_collection.children[col.name].hide_viewport = True

					wrap = self[ ob ]
					wrap.objects = []
					for child in col.objects:
						wrap.objects.append( self[child] )

					return ob

		def remove_doubles(self, ob):
			self.set_active(ob)
			bpy.ops.object.mode_set(mode='EDIT')
			bpy.ops.mesh.remove_doubles()  ## use_unselected
			bpy.ops.object.mode_set(mode='OBJECT')

		def draw_type(self, ob, T):
			if BLENDER27:
				ob.draw_type = T
			else:
				ob.display_type = T

		def shared_material(self, color, **kwargs):
			kwargs['color']=color
			return self.new_material(name=str(color), **kwargs)

		def new_material(self, name='unnamed-material',diffuse_color=[1,1,1],color=None,vertex_color=False,specular_intensity=0,texture=None,emit=0, water=False):
			if color:
				diffuse_color = color
			if water:
				diffuse_color = [.1,.1, 1]
			if name in bpy.data.materials:
				#print('WARN: material already exists')
				return bpy.data.materials[name]

			print('making new material: ' + name)
			m = bpy.data.materials.new(name=name)
			if BLENDER27:
				m.diffuse_color = diffuse_color
				if vertex_color:
					m.use_vertex_color_light = True  ## TODO port to new blender2.8 vertex color materials ##
					m.use_vertex_color_paint = True
				if texture:
					m.texture_slots.create(0)
					m.texture_slots[0].texture = texture
				if emit:
					m.emit = emit
			else:
				if len(diffuse_color) == 4:
					m.diffuse_color = diffuse_color
				else:
					m.diffuse_color = diffuse_color + [1]
				if water:
					m.metallic = 0.5
					m.specular_intensity = 0.95
					m.roughness = 0.2

			m.specular_intensity = specular_intensity
			return m

		def __getitem__(self, obj):
			if type(obj) is str:
				if obj in bpy.data.objects:
					obj = bpy.data.objects[obj]
				else:
					return None
			if isinstance(obj, BlenderObjectWrapper):
				return obj
			if obj in self.Objects:
				return self.Objects[ obj ]
			w = BlenderObjectWrapper(obj)
			self.Objects[ obj ] = w
			return w

		def set_active(self, ob):
			if BLENDER27:
				ob.select = True
				bpy.context.scene.objects.active = ob
			else:
				ob.select_set(True)
				bpy.context.view_layer.objects.active = ob

		def join(self, obs):
			self.set_active(obs[0])
			for o in bpy.data.objects:
				self.unselect(o)
			for o in obs:
				self.select(o)
			bpy.ops.object.join()
			return bpy.context.active_object

		def make_bush(self, radius=1.0, pinch=-0.7, push=2, red=0, green=1, blue=0, smooth=True, vines=False):
			plant = self.new_prim('sphere', radius=radius, color=[red,green,blue])
			if smooth:
				for poly in plant.data.polygons: poly.use_smooth = True

			for vert in plant.data.vertices:
				vert.co *= uniform(pinch, push)

			if vines:
				mod = self[plant].modify('SUBSURF')
				mod.render_levels = 1
				mod = self[plant].modify("WIREFRAME")
				mod = self[plant].modify("SMOOTH")
				mod.factor = uniform(1,3)

			return plant

		def make_circle(self, radius=1.0, material=None, object=None, noise=0, z=0):
			stroke = self.new_stroke(object, points=90, material=material)
			points = stroke.points
			for i, point in enumerate(points):
				points[i].co = [
					(math.sin(math.radians(i*4)) * radius), 
					(math.cos(math.radians(i*4)) * radius),
					z,
				]
				if noise:
					points[i].co.x += uniform(-noise,noise)
					points[i].co.y += uniform(-noise,noise)

			return stroke

		def make_square(self, radius=1.0, material=None, object=None, width=1.0, orientation='TOP', x=0, taper_bottom=1.0, noise=0, z=0):
			stroke = self.new_stroke(object, points=4, material=material)
			points = stroke.points

			points[0].co.x = -radius *width * taper_bottom
			points[1].co.x = radius  *width * taper_bottom
			points[3].co.x = -radius *width
			points[2].co.x = radius  *width

			if orientation=='FRONT':
				points[0].co.z = -radius
				points[1].co.z = -radius
				points[2].co.z = radius
				points[3].co.z = radius
			else:
				points[0].co.y = -radius
				points[1].co.y = -radius
				points[2].co.y = radius
				points[3].co.y = radius

			for i in range(4):
				points[i].co.x += x
				points[i].co.z += z
				points[i].co *= uniform(-noise, noise) + 1

			return stroke

		def copy_stroke(self, stroke, object, z=0):
			copy = self.new_stroke(object)
			copy.points.add( count=len(stroke.points) )

			if BLENDER27:
				copy.colorname = stroke.colorname
			else:
				copy.material_index = stroke.material_index

			for i,p in enumerate(stroke.points):
				copy.points[i].co = p.co
				copy.points[i].co.z += z
			return copy

		def make_grass(self, steps_x=8, steps_y=4, noise=0.1, height=1, z=0):
			ob = self.new_gpen(mode='2D')
			ob.name = 'grass'
			y = -1.1
			for sx in range(steps_x):
				stroke = self.new_stroke(ob, material="GRASS")
				stroke.points.add( count=20 )
				x = -1
				for i, point in enumerate(stroke.points):
					stroke.points[i].co = [ x, y+uniform(-0.1,.1), (random()*height)+z ]
					x += 0.1
				y += 1.0 / steps_x * 2
			return ob

		def make_grass_ring(self, radius=1.0, rings=None, noise=0.1, pond=False, ducks=0, shadow=False, z=0.01, subobjects=None):
			# grass around pond
			ob = self.new_gpen(mode='2D')
			ob.name = 'grass_ring'
			if rings is None:
				rings = int(radius*0.25) or 2

			for ring in range( rings ):
				ring *= radius / 2
				stroke = self.new_stroke(ob, material="DARK_GRASS")
				stroke.points.add( count=90 )
				points = stroke.points
				for i, point in enumerate(points):
					points[i].co = [
						(math.sin(math.radians(i*4)) * (radius+ring)), 
						(math.cos(math.radians(i*4)) * (radius+ring)),
						z,
					]
					#points[i].co *= uniform(0.7,radius*1.3)
					points[i].co *= uniform(0.8,1.1)
				z += 0.01


			for ring in range( rings ):
				#if random() > 0.8:
				#	stroke = api.new_stroke(ob, "DARK_GRASS")
				#else:

				ring *= radius / 4

				deg = 0
				#steps = 90
				# Add points
				for step in range(4):
					stroke = self.new_stroke(ob, material="GRASS")
					stroke.points.add( count=22 )
					points = stroke.points
					for i, point in enumerate(points):
						z = random() * radius * 0.5
						z += 0.01
						points[i].co = [
							(math.sin(math.radians(deg*4)) * (radius+ring)), 
							(math.cos(math.radians(deg*4)) * (radius+ring)),
							z,
						]
						if points[i].co.z > 0.5:
							points[i].co.x += uniform(-noise,noise)
							points[i].co.y += uniform(-noise,noise)
						deg += 1
					points[0].co.z = 0.01
					points[-1].co.z = 0.01

			if pond:
				circle = self.make_circle(radius=radius*0.7, material="WATER", object=ob, noise=0.1, z=-0.2)
				self.copy_stroke(circle, ob, z=0.1)

			if shadow:
				for i in range(6):
					rad = 0.1 + (i*0.2)
					circle = self.make_circle(radius=rad, material="SHADOW", object=ob)

			if ducks:
				for i in range(ducks):
					duck = self.link_file('DUCK')
					duck.parent = ob
					duck.location.x = uniform(-1,1)
					duck.location.y = uniform(-1,1)
					scale = uniform(0.3, 1.0)
					duck.location.z += 0.2
					duck.scale *= scale
					if type(subobjects) is list:
						subobjects.append( duck )

			return ob


	## Blender Cell Fracture - from Blender2.80.0
	## by: ideasman42, phymec, Sergey Sharybin
	## forked by: DJRaptor
	from math import sqrt
	import mathutils, bmesh
	from mathutils import Vector

	def points_as_bmesh_cells(verts, points, points_scale=None, margin_bounds=0.05, margin_cell=0.0):

		cells = []

		points_sorted_current = [p for p in points]
		plane_indices = []
		vertices = []

		if points_scale is not None:
			points_scale = tuple(points_scale)
		if points_scale == (1.0, 1.0, 1.0):
			points_scale = None

		# there are many ways we could get planes - convex hull for eg
		# but it ends up fastest if we just use bounding box
		if 1:
			xa = [v[0] for v in verts]
			ya = [v[1] for v in verts]
			za = [v[2] for v in verts]

			xmin, xmax = min(xa) - margin_bounds, max(xa) + margin_bounds
			ymin, ymax = min(ya) - margin_bounds, max(ya) + margin_bounds
			zmin, zmax = min(za) - margin_bounds, max(za) + margin_bounds
			convexPlanes = [
				Vector((+1.0, 0.0, 0.0, -xmax)),
				Vector((-1.0, 0.0, 0.0, +xmin)),
				Vector((0.0, +1.0, 0.0, -ymax)),
				Vector((0.0, -1.0, 0.0, +ymin)),
				Vector((0.0, 0.0, +1.0, -zmax)),
				Vector((0.0, 0.0, -1.0, +zmin)),
				]

		for i, point_cell_current in enumerate(points):
			planes = [None] * len(convexPlanes)
			for j in range(len(convexPlanes)):
				planes[j] = convexPlanes[j].copy()
				planes[j][3] += planes[j].xyz.dot(point_cell_current)
			distance_max = 10000000000.0  # a big value!

			points_sorted_current.sort(key=lambda p: (p - point_cell_current).length_squared)

			for j in range(1, len(points)):
				normal = points_sorted_current[j] - point_cell_current
				nlength = normal.length

				if points_scale is not None:
					normal_alt = normal.copy()
					normal_alt.x *= points_scale[0]
					normal_alt.y *= points_scale[1]
					normal_alt.z *= points_scale[2]

					# rotate plane to new distance
					# should always be positive!! - but abs incase
					scalar = normal_alt.normalized().dot(normal.normalized())
					# assert(scalar >= 0.0)
					nlength *= scalar
					normal = normal_alt

				if nlength > distance_max:
					break

				plane = normal.normalized()
				plane.resize_4d()
				plane[3] = (-nlength / 2.0) + margin_cell
				planes.append(plane)

				vertices[:], plane_indices[:] = mathutils.geometry.points_in_planes(planes)
				if len(vertices) == 0:
					log('WARN: cell fracture - no verts')
					break
				elif '--debug-cell-fracture' in sys.argv:
					log(vertices)

				if len(plane_indices) != len(planes):
					planes[:] = [planes[k] for k in plane_indices]

				# for comparisons use length_squared and delay
				# converting to a real length until the end.
				distance_max = 10000000000.0  # a big value!
				for v in vertices:
					distance = v.length_squared
					if distance_max < distance:
						distance_max = distance
				distance_max = sqrt(distance_max)  # make real length
				distance_max *= 2.0

			if len(vertices) == 0:
				log('WARN: cell fracture - no verts')
				continue
			elif '--debug-cell-fracture' in sys.argv:
				log(vertices)

			cells.append((point_cell_current, vertices[:]))
			if '--debug-cell-fracture' in sys.argv:
				log('cell fracture: about to del vertices')
			del vertices[:]
		log('cell fracture: returning cells...')
		if '--debug-cell-fracture' in sys.argv:
			log(cells)
		return cells

	def _points_from_object(depsgraph, scene, obj, source):

		_source_all = {
			'PARTICLE_OWN', 'PARTICLE_CHILD',
			'PENCIL',
			'VERT_OWN', 'VERT_CHILD',
			}

		# print(source - _source_all)
		# print(source)
		assert(len(source | _source_all) == len(_source_all))
		assert(len(source))

		points = []

		def edge_center(mesh, edge):
			v1, v2 = edge.vertices
			return (mesh.vertices[v1].co + mesh.vertices[v2].co) / 2.0

		def poly_center(mesh, poly):
			from mathutils import Vector
			co = Vector()
			tot = 0
			for i in poly.loop_indices:
				co += mesh.vertices[mesh.loops[i].vertex_index].co
				tot += 1
			return co / tot
		if BLENDER27:
			def points_from_verts(obj):
				"""Takes points from _any_ object with geometry"""
				if obj.type == 'MESH':
					mesh = obj.data
					matrix = obj.matrix_world.copy()
					points.extend([matrix * v.co for v in mesh.vertices])
				else:
					try:
						mesh = ob.to_mesh(scene=bpy.context.scene,
										  apply_modifiers=True,
										  settings='PREVIEW')
					except:
						mesh = None

					if mesh is not None:
						matrix = obj.matrix_world.copy()
						points.extend([matrix * v.co for v in mesh.vertices])
						bpy.data.meshes.remove(mesh)

			def points_from_particles(obj):
				points.extend([p.location.copy()
							   for psys in obj.particle_systems
							   for p in psys.particles])
		else:
			def points_from_verts(obj):
				"""Takes points from _any_ object with geometry"""
				if obj.type == 'MESH':
					mesh = obj.data
					matrix = obj.matrix_world.copy()
					#points.extend([matrix @ v.co for v in mesh.vertices])
					if BLENDER27:
						points.extend([matrix * v.co for v in mesh.vertices])
					else:
						points.extend([matrix.__matmul__(v.co) for v in mesh.vertices])
				else:
					ob_eval = ob.evaluated_get(depsgraph)
					try:
						mesh = ob_eval.to_mesh()
					except:
						mesh = None

					if mesh is not None:
						matrix = obj.matrix_world.copy()
						#points.extend([matrix @ v.co for v in mesh.vertices])
						if BLENDER27:
							points.extend([matrix * v.co for v in mesh.vertices])
						else:
							points.extend([matrix.__matmul__(v.co) for v in mesh.vertices])
						ob_eval.to_mesh_clear()

			def points_from_particles(obj):
				obj_eval = obj.evaluated_get(depsgraph)
				points.extend([p.location.copy()
							   for psys in obj_eval.particle_systems
							   for p in psys.particles])

		# geom own
		if 'VERT_OWN' in source:
			points_from_verts(obj)

		# geom children
		if 'VERT_CHILD' in source:
			for obj_child in obj.children:
				points_from_verts(obj_child)

		# geom particles
		if 'PARTICLE_OWN' in source:
			points_from_particles(obj)

		if 'PARTICLE_CHILD' in source:
			for obj_child in obj.children:
				points_from_particles(obj_child)

		# grease pencil
		def get_points(stroke):
			return [point.co.copy() for point in stroke.points]

		def get_splines(gp):
			if gp.layers.active:
				frame = gp.layers.active.active_frame
				return [get_points(stroke) for stroke in frame.strokes]
			else:
				return []

		if 'PENCIL' in source:
			# Used to be from object in 2.7x, now from scene.
			gp = scene.grease_pencil
			if gp:
				points.extend([p for spline in get_splines(gp)
								 for p in spline])

		log("Found %d points" % len(points))

		return points


	def cell_fracture_objects(context, obj,
							  source={'PARTICLE_OWN'},
							  source_limit=0,
							  source_noise=0.0,
							  clean=True,
							  # operator options
							  use_smooth_faces=False,
							  use_data_match=False,
							  use_debug_points=False,
							  margin=0.0,
							  material_index=0,
							  cell_scale=(1.0, 1.0, 1.0),
							  ):

		scene = context.scene
		if BLENDER27:
			depsgraph = collection = view_layer = None
		else:
			depsgraph = context.evaluated_depsgraph_get()
			collection = context.collection
			view_layer = context.view_layer

		# -------------------------------------------------------------------------
		# GET POINTS

		points = _points_from_object(depsgraph, scene, obj, source)

		if not points:
			# print using fallback
			points = _points_from_object(depsgraph, scene, obj, {'VERT_OWN'})

		if not points:
			log("WARN: no points found")
			return []

		# apply optional clamp
		if source_limit != 0 and source_limit < len(points):
			shuffle(points)
			points[source_limit:] = []

		## NOT PY2 compatible
		## SyntaxError: can not delete variable 'to_tuple' referenced in nested scope
		# saddly we cant be sure there are no doubles
		#from mathutils import Vector
		#to_tuple = Vector.to_tuple
		#points = list({to_tuple(p, 4): p for p in points}.values())
		#del to_tuple
		#del Vector

		# end remove doubles
		# ------------------

		if source_noise > 0.0:
			# boundbox approx of overall scale
			from mathutils import Vector
			matrix = obj.matrix_world.copy()
			#bb_world = [matrix @ Vector(v) for v in obj.bound_box]
			if BLENDER27:
				bb_world = [matrix * Vector(v) for v in obj.bound_box]
			else:
				bb_world = [matrix.__matmul__(Vector(v)) for v in obj.bound_box]

			scalar = source_noise * ((bb_world[0] - bb_world[6]).length / 2.0)

			from mathutils.noise import random_unit_vector

			points[:] = [p + (random_unit_vector() * (scalar * random())) for p in points]

		if use_debug_points:
			bm = bmesh.new()
			for p in points:
				bm.verts.new(p)
			mesh_tmp = bpy.data.meshes.new(name="DebugPoints")
			bm.to_mesh(mesh_tmp)
			bm.free()
			obj_tmp = bpy.data.objects.new(name=mesh_tmp.name, object_data=mesh_tmp)
			collection.objects.link(obj_tmp)
			del obj_tmp, mesh_tmp

		mesh = obj.data
		matrix = obj.matrix_world.copy()
		#verts = [matrix @ v.co for v in mesh.vertices]
		if BLENDER27:
			verts = [matrix * v.co for v in mesh.vertices]
		else:
			verts = [matrix.__matmul__(v.co) for v in mesh.vertices]

		cells = points_as_bmesh_cells(
			verts,
			points,
			cell_scale,
			margin_cell=margin)

		# some hacks here :S
		cell_name = obj.name + "_cell"

		objects = []

		for center_point, cell_points in cells:
			# ---------------------------------------------------------------------
			# BMESH

			# create the convex hulls
			bm = bmesh.new()

			# WORKAROUND FOR CONVEX HULL BUG/LIMIT
			# XXX small noise
			def R():
				return (random() - 0.5) * 0.001
			# XXX small noise

			for i, co in enumerate(cell_points):

				# XXX small noise
				co.x += R()
				co.y += R()
				co.z += R()
				# XXX small noise

				bm_vert = bm.verts.new(co)

			import mathutils
			## this seems to fail to remove doubles
			bmesh.ops.remove_doubles(bm, verts=bm.verts, dist=0.05)
			try:
				bmesh.ops.convex_hull(bm, input=bm.verts)
			except RuntimeError:
				import traceback
				traceback.print_exc()

			if clean:
				bm.normal_update()
				try:
					bmesh.ops.dissolve_limit(bm, verts=bm.verts, angle_limit=0.001)
				except RuntimeError:
					import traceback
					traceback.print_exc()
			# Smooth faces will remain only inner faces, after appling boolean modifier.
			if use_smooth_faces:
				for bm_face in bm.faces:
					bm_face.smooth = True

			if material_index != 0:
				for bm_face in bm.faces:
					bm_face.material_index = material_index

			# ---------------------------------------------------------------------
			# MESH
			mesh_dst = bpy.data.meshes.new(name=cell_name)

			bm.to_mesh(mesh_dst)
			bm.free()
			del bm

			if use_data_match:
				# match materials and data layers so boolean displays them
				# currently only materials + data layers, could do others...
				mesh_src = obj.data
				for mat in mesh_src.materials:
					mesh_dst.materials.append(mat)
				for lay_attr in ("vertex_colors", "uv_layers"):
					lay_src = getattr(mesh_src, lay_attr)
					lay_dst = getattr(mesh_dst, lay_attr)
					for key in lay_src.keys():
						lay_dst.new(name=key)

			# ---------------------------------------------------------------------
			# OBJECT

			obj_cell = bpy.data.objects.new(name=cell_name, object_data=mesh_dst)
			if BLENDER27:
				bpy.context.scene.objects.link(obj_cell)
			else:
				collection.objects.link(obj_cell)

			#api.remove_doubles( obj_cell )
			# scene.objects.active = obj_cell
			obj_cell.location = center_point
			objects.append(obj_cell)

			# support for object materials
			if use_data_match:
				for i in range(len(mesh_dst.materials)):
					slot_src = obj.material_slots[i]
					slot_dst = obj_cell.material_slots[i]

					slot_dst.link = slot_src.link
					slot_dst.material = slot_src.material

		if BLENDER27:
			scene.update()
		else:
			view_layer.update()

		return objects


	def cell_fracture_boolean(context, obj, objects, use_debug_bool=False, clean=True, use_island_split=False, use_interior_hide=False, level=0, remove_doubles=True):
		objects_boolean = []
		scene = context.scene
		if BLENDER27:
			collection = view_layer = None
		else:
			collection = context.collection
			view_layer = context.view_layer

		if use_interior_hide and level == 0:
			# only set for level 0
			obj.data.polygons.foreach_set("hide", [False] * len(obj.data.polygons))

		for obj_cell in objects:
			mod = obj_cell.modifiers.new(name="Boolean", type='BOOLEAN')
			mod.object = obj
			mod.operation = 'INTERSECT'
			if not use_debug_bool:
				if use_interior_hide:
					obj_cell.data.polygons.foreach_set("hide", [True] * len(obj_cell.data.polygons))

		# Calculates all booleans at once (faster).
		if BLENDER27:
			depsgraph = None
		else:
			depsgraph = context.evaluated_depsgraph_get()

		for obj_cell in objects:
			log(obj_cell)
			if not use_debug_bool:
				if BLENDER27:
					mesh_new = obj_cell.to_mesh(scene,apply_modifiers=True,settings='PREVIEW')

				else:
					obj_cell_eval = obj_cell.evaluated_get(depsgraph)
					mesh_new = bpy.data.meshes.new_from_object(obj_cell_eval)
				mesh_old = obj_cell.data
				obj_cell.data = mesh_new
				obj_cell.modifiers.remove(obj_cell.modifiers[-1])

				if False:  ## DJRaptor note: lots of crashing if this is true
					# remove if not valid
					if not mesh_old.users:
						log('NOT removing old.users....')
						##bpy.data.meshes.remove(mesh_old)  ## this crashes the original cell frature script!
					if not mesh_new.vertices:
						log('unlinking....')
						collection.objects.unlink(obj_cell)
						if not obj_cell.users:
							bpy.data.objects.remove(obj_cell)
							obj_cell = None
							if not mesh_new.users:
								bpy.data.meshes.remove(mesh_new)
								mesh_new = None
					# avoid unneeded bmesh re-conversion
					if mesh_new is not None:
						bm = None

						if clean:
							log('cleaning...')
							if bm is None:  # ok this will always be true for now...
								bm = bmesh.new()
								bm.from_mesh(mesh_new)
							bm.normal_update()
							try:
								bmesh.ops.dissolve_limit(bm, verts=bm.verts, edges=bm.edges, angle_limit=0.001)
							except RuntimeError:
								import traceback
								traceback.print_exc()

						if remove_doubles:
							if bm is None:
								bm = bmesh.new()
								bm.from_mesh(mesh_new)
							bmesh.ops.remove_doubles(bm, verts=bm.verts, dist=0.005)

						if bm is not None:
							bm.to_mesh(mesh_new)
							bm.free()

					del mesh_new
					del mesh_old

			if obj_cell is not None:
				objects_boolean.append(obj_cell)

		if (not use_debug_bool) and use_island_split:
			# this is ugly and Im not proud of this - campbell
			if BLENDER27:
				base = None
				for base in scene.object_bases:
					base.select = False
				for obj_cell in objects_boolean:
					obj_cell.select = True
			else:
				for ob in view_layer.objects:
					ob.select_set(False)
				for obj_cell in objects_boolean:
					obj_cell.select_set(True)

			bpy.ops.mesh.separate(type='LOOSE')
			if BLENDER27:
				objects_boolean[:] = [obj_cell for obj_cell in scene.objects if obj_cell.select]
			else:
				objects_boolean[:] = [obj_cell for obj_cell in view_layer.objects if obj_cell.select_get()]

		if BLENDER27:
			scene.update()
		else:
			context.view_layer.update()
		log('cell fracture: bools OK')
		return objects_boolean


	def cell_fracture_interior_handle(objects, use_interior_vgroup=False, use_sharp_edges=False, use_sharp_edges_apply=False):
		log("""cell fracture: Run after doing _all_ booleans""")

		assert(use_interior_vgroup or use_sharp_edges or use_sharp_edges_apply)

		for obj_cell in objects:
			mesh = obj_cell.data
			bm = bmesh.new()
			bm.from_mesh(mesh)

			if use_interior_vgroup:
				for bm_vert in bm.verts:
					bm_vert.tag = True
				for bm_face in bm.faces:
					if not bm_face.hide:
						for bm_vert in bm_face.verts:
							bm_vert.tag = False

				# now add all vgroups
				defvert_lay = bm.verts.layers.deform.verify()
				for bm_vert in bm.verts:
					if bm_vert.tag:
						bm_vert[defvert_lay][0] = 1.0

				# add a vgroup
				obj_cell.vertex_groups.new(name="Interior")

			if use_sharp_edges:
				for bm_edge in bm.edges:
					if len({bm_face.hide for bm_face in bm_edge.link_faces}) == 2:
						bm_edge.smooth = False

				if use_sharp_edges_apply:
					edges = [edge for edge in bm.edges if edge.smooth is False]
					if edges:
						bm.normal_update()
						bmesh.ops.split_edges(bm, edges=edges)

			for bm_face in bm.faces:
				bm_face.hide = False

			bm.to_mesh(mesh)
			#print('cell fracture: calling bm.free()')
			bm.free()
			#print('cell fracture: bm.free OK')


	def cell_fracture_mesh(context, obj, level, **kw):
		# pull out some args
		kw_copy = kw.copy()
		use_recenter = kw_copy.pop("use_recenter")
		use_remove_original = kw_copy.pop("use_remove_original")
		recursion = kw_copy.pop("recursion")
		recursion_source_limit = kw_copy.pop("recursion_source_limit")
		recursion_clamp = kw_copy.pop("recursion_clamp")
		recursion_chance = kw_copy.pop("recursion_chance")
		recursion_chance_select = kw_copy.pop("recursion_chance_select")
		collection_name = kw_copy.pop("collection_name")
		use_island_split = kw_copy.pop("use_island_split")
		use_debug_bool = kw_copy.pop("use_debug_bool")
		use_interior_vgroup = kw_copy.pop("use_interior_vgroup")
		use_sharp_edges = kw_copy.pop("use_sharp_edges")
		use_sharp_edges_apply = kw_copy.pop("use_sharp_edges_apply")

		scene = context.scene
		if BLENDER27:
			collection = None
		else:
			collection = context.collection

		if level != 0:
			kw_copy["source_limit"] = recursion_source_limit


		# not essential but selection is visual distraction.
		if BLENDER27:
			obj.select = False
			obj.draw_type = 'WIRE'

		else:
			obj.select_set(False)
			obj.display_type = 'WIRE'

		log('calling cell_fracture_objects...')
		objects = cell_fracture_objects(context, obj, **kw_copy)
		log('calling cell_fracture_boolean...')
		objects = cell_fracture_boolean(
			context, obj, objects,
			use_island_split=use_island_split,
			use_interior_hide=(use_interior_vgroup or use_sharp_edges),
			use_debug_bool=use_debug_bool,
			level=level,
		)

		log('cell_fracture_boolean OK')
		# must apply after boolean.
		if use_recenter:
			bpy.ops.object.origin_set({"selected_editable_objects": objects},
									  type='ORIGIN_GEOMETRY', center='MEDIAN')

		#----------
		# Recursion
		if level == 0:
			for level_sub in range(1, recursion + 1):

				objects_recurse_input = [(i, o) for i, o in enumerate(objects)]

				if recursion_chance != 1.0:
					from mathutils import Vector
					if recursion_chance_select == 'RANDOM':
						shuffle(objects_recurse_input)
					elif recursion_chance_select in {'SIZE_MIN', 'SIZE_MAX'}:
						objects_recurse_input.sort(key=lambda ob_pair:
							(Vector(ob_pair[1].bound_box[0]) -
							 Vector(ob_pair[1].bound_box[6])).length_squared)
						if recursion_chance_select == 'SIZE_MAX':
							objects_recurse_input.reverse()
					elif recursion_chance_select in {'CURSOR_MIN', 'CURSOR_MAX'}:
						c = scene.cursor.location.copy()
						objects_recurse_input.sort(key=lambda ob_pair:
							(ob_pair[1].location - c).length_squared)
						if recursion_chance_select == 'CURSOR_MAX':
							objects_recurse_input.reverse()

					objects_recurse_input[int(recursion_chance * len(objects_recurse_input)):] = []
					objects_recurse_input.sort()

				# reverse index values so we can remove from original list.
				objects_recurse_input.reverse()

				objects_recursive = []
				for i, obj_cell in objects_recurse_input:
					assert(objects[i] is obj_cell)
					objects_recursive += main_object(context, obj_cell, level_sub, **kw)
					if use_remove_original:
						collection.objects.unlink(obj_cell)
						del objects[i]
					if recursion_clamp and len(objects) + len(objects_recursive) >= recursion_clamp:
						break
				objects.extend(objects_recursive)

				if recursion_clamp and len(objects) > recursion_clamp:
					break

		#--------------
		# Level Options
		if level == 0:
			# import pdb; pdb.set_trace()
			if use_interior_vgroup or use_sharp_edges:
				cell_fracture_interior_handle(
					objects,
					use_interior_vgroup=use_interior_vgroup,
					use_sharp_edges=use_sharp_edges,
					use_sharp_edges_apply=use_sharp_edges_apply,
				)

		#--------------
		# Scene Options

		# group
		if collection_name:
			group = bpy.data.collections.get(collection_name)
			if group is None:
				group = bpy.data.collections.new(collection_name)
				collection.children.link(group)
			group_objects = group.objects[:]
			for obj_cell in objects:
				if obj_cell not in group_objects:
					collection.objects.unlink(obj_cell)
					group.objects.link(obj_cell)

		# testing only!
		# obj.hide = True
		return objects


	def cell_fracture_main(context, **kw):
		import time
		t = time.time()
		#objects_context = context.selected_editable_objects

		kw_copy = kw.copy()

		# mass
		mass_mode = kw_copy.pop("mass_mode")
		mass = kw_copy.pop("mass")
		obname = kw_copy.pop('object_name')
		if not obname and bpy.context.active_object:
			obname = bpy.context.active_object.name
		if 'darken_inner_verts' in kw_copy:
			darken_inner_verts = kw_copy.pop('darken_inner_verts')
		else:
			darken_inner_verts = False

		objects = []
		#for obj in objects_context:
		#	if obj.type == 'MESH':
		#		objects += main_object(context, obj, 0, **kw_copy)
		if obname.startswith('OB'):
			obname = obname[2:]

		obj = bpy.data.objects[obname]
		if obj.type == 'MESH':
			if BLENDER27:
				if obj.draw_type != 'WIRE' and not obj.ode_broken:
					obj.ode_broken = True
					objects += cell_fracture_mesh(context, obj, 0, **kw_copy)
			else:
				if obj.display_type != 'WIRE' and not obj.ode_broken:
					obj.ode_broken = True
					objects += cell_fracture_mesh(context, obj, 0, **kw_copy)
		elif obj.type == 'META':
			obj.ode_broken = True
			for i in range(12):
				ball = bpy.data.objects.new(name=obj.name, object_data=obj.data)
				context.collection.objects.link(ball)
				ball.location = obj.location
				ball.location.x += uniform(-obj.scale.x * 0.25, obj.scale.x * 0.25)
				ball.location.y += uniform(-obj.scale.y * 0.25, obj.scale.y * 0.25)
				ball.location.z += uniform(-obj.scale.z * 0.25, obj.scale.z * 0.25)
				ball.location.z += uniform(0, 2)
				ball.scale = obj.scale
				ball.scale *= uniform(0.3, 0.6)
				objects.append(ball)
			obj.scale *= 0.25

		log('cell fracture: main_object calls OK')
		bpy.ops.object.select_all(action='DESELECT')  ## unstable without this
		log('cell fracture: deselect all OK')

		msg = []
		for cell in objects:
			cell.rotation_mode = 'QUATERNION'
			#cell.ode_use_bounce = obj.ode_use_bounce
			#cell.ode_bounce = obj.ode_bounce

			## pipe here to pyqt/ode
			#_bpy.ode_add_object( cell )

			#if obj.ode_recursive_break_steps:
			#	cell.ode_recursive_break_steps = obj.ode_recursive_break_steps-1
			#	if cell.ode_recursive_break_steps:
			#		cell.ode_breakable = True
			x,y,z = cell.location
			x = round(x,2)
			y = round(y,2)
			z = round(z,2)
			msg.append({'name':cell.name, 'pos':[x,y,z]})

			if darken_inner_verts and cell.type=='MESH':
				if len(cell.data.vertex_colors) == 0:
					vc = cell.data.vertex_colors.new()
				else:
					vc = cell.data.vertex_colors[0]
				i = 0
				for poly in cell.data.polygons:
					for idx in poly.loop_indices:
						loop = cell.data.loops[idx]
						vert = cell.data.vertices[ loop.vertex_index ]
						dist = ((vert.co+cell.location) - obj.location).length
						if dist < 0.75:
							vc.data[i].color = [0,0,0, 1]
						else:
							vc.data[i].color = [1,1,1, 1]
						i += 1

		if _qt_proc:
			_qt_proc.stdin.write( json.dumps(msg).encode('utf-8') + b'\n' )
			_qt_proc.stdin.flush()


		if obj.ode_break_script:
			scope = {}
			scope.update(globals())
			scope['breakable'] = obj
			scope['shards']    = objects
			exec( obj.ode_break_script.as_string(), scope, scope )

		return objects

	@bpy.utils.register_class
	class PanjeaFractureCell(bpy.types.Operator):
		bl_idname = "panjea.cell_fracture"
		bl_label = "Cell fracture selected mesh objects"
		bl_options = {'PRESET'}

		# -------------------------------------------------------------------------
		# Source Options
		source= bpy.props.EnumProperty(
				name="Source",
				items=(('VERT_OWN', "Own Verts", "Use own vertices"),
					   ('VERT_CHILD', "Child Verts", "Use child object vertices"),
					   ('PARTICLE_OWN', "Own Particles", ("All particle systems of the "
														  "source object")),
					   ('PARTICLE_CHILD', "Child Particles", ("All particle systems of the "
															  "child objects")),
					   ('PENCIL', "Annotation Pencil", "Annotation Grease Pencil."),
					   ),
				options={'ENUM_FLAG'},
				default={'PARTICLE_OWN'},
				)

		source_limit= bpy.props.IntProperty(
				name="Source Limit",
				description="Limit the number of input points, 0 for unlimited",
				min=0, max=5000,
				default=100,
				)

		source_noise= bpy.props.FloatProperty(
				name="Noise",
				description="Randomize point distribution",
				min=0.0, max=1.0,
				default=0.5,
				)

		cell_scale= bpy.props.FloatVectorProperty(
				name="Scale",
				description="Scale Cell Shape",
				size=3,
				min=0.0, max=1.0,
				default=(1.0, 1.0, 1.0),
				)

		# -------------------------------------------------------------------------
		# Recursion

		recursion= bpy.props.IntProperty(
				name="Recursion",
				description="Break shards recursively",
				min=0, max=5000,
				default=0,
				)

		recursion_source_limit= bpy.props.IntProperty(
				name="Source Limit",
				description="Limit the number of input points, 0 for unlimited (applies to recursion only)",
				min=0, max=5000,
				default=8,
				)

		recursion_clamp= bpy.props.IntProperty(
				name="Clamp Recursion",
				description="Finish recursion when this number of objects is reached (prevents recursing for extended periods of time), zero disables",
				min=0, max=10000,
				default=250,
				)

		recursion_chance= bpy.props.FloatProperty(
				name="Random Factor",
				description="Likelihood of recursion",
				min=0.0, max=1.0,
				default=0.25,
				)

		recursion_chance_select= bpy.props.EnumProperty(
				name="Recurse Over",
				items=(('RANDOM', "Random", ""),
					   ('SIZE_MIN', "Small", "Recursively subdivide smaller objects"),
					   ('SIZE_MAX', "Big", "Recursively subdivide bigger objects"),
					   ('CURSOR_MIN', "Cursor Close", "Recursively subdivide objects closer to the cursor"),
					   ('CURSOR_MAX', "Cursor Far", "Recursively subdivide objects farther from the cursor"),
					   ),
				default='SIZE_MIN',
				)

		# -------------------------------------------------------------------------
		# Mesh Data Options

		use_smooth_faces= bpy.props.BoolProperty(
				name="Smooth Interior",
				description="Smooth Faces of inner side",
				default=False,
				)

		use_sharp_edges= bpy.props.BoolProperty(
				name="Sharp Edges",
				description="Set sharp edges when disabled",
				default=True,
				)

		use_sharp_edges_apply= bpy.props.BoolProperty(
				name="Apply Split Edge",
				description="Split sharp hard edges",
				default=True,
				)

		use_data_match= bpy.props.BoolProperty(
				name="Match Data",
				description="Match original mesh materials and data layers",
				default=True,
				)

		use_island_split= bpy.props.BoolProperty(
				name="Split Islands",
				description="Split disconnected meshes",
				default=True,
				)

		margin= bpy.props.FloatProperty(
				name="Margin",
				description="Gaps for the fracture (gives more stable physics)",
				min=0.0, max=1.0,
				default=0.001,
				)

		material_index= bpy.props.IntProperty(
				name="Material",
				description="Material index for interior faces",
				default=1,
				)

		use_interior_vgroup= bpy.props.BoolProperty(
				name="Interior VGroup",
				description="Create a vertex group for interior verts",
				default=False,
				)

		# -------------------------------------------------------------------------
		# Physics Options

		mass_mode= bpy.props.EnumProperty(
				name="Mass Mode",
				items=(('VOLUME', "Volume", "Objects get part of specified mass based on their volume"),
					   ('UNIFORM', "Uniform", "All objects get the specified mass"),
					   ),
				default='VOLUME',
				)

		mass= bpy.props.FloatProperty(
				name="Mass",
				description="Mass to give created objects",
				min=0.001, max=1000.0,
				default=1.0,
				)


		# -------------------------------------------------------------------------
		# Object Options

		use_recenter= bpy.props.BoolProperty(
				name="Recenter",
				description="Recalculate the center points after splitting",
				default=True,
				)

		use_remove_original= bpy.props.BoolProperty(
				name="Remove Original",
				description="Removes the parents used to create the shatter",
				default=False,
				)

		# -------------------------------------------------------------------------
		# Scene Options
		#
		# .. different from object options in that this controls how the objects
		#    are setup in the scene.

		collection_name= bpy.props.StringProperty(
				name="Collection",
				description="Create objects in a collection "
							"(use existing or create new)",
				)

		object_name= bpy.props.StringProperty(
				name="Object Name",
				description="object to shatter"
				)

		# -------------------------------------------------------------------------
		# Debug
		use_debug_points= bpy.props.BoolProperty(
				name="Debug Points",
				description="Create mesh data showing the points used for fracture",
				default=False,
				)


		use_debug_bool= bpy.props.BoolProperty(
				name="Debug Boolean",
				description="Skip applying the boolean modifier",
				default=False,
				)

		def execute(self, context):
			log('panjea.cell_fracture...')
			keywords = self.as_keywords()  # ignore=("blah",)
			cell_fracture_main(context, **keywords)
			return {'FINISHED'}


		def invoke(self, context, event):
			# print(self.recursion_chance_select)
			wm = context.window_manager
			return wm.invoke_props_dialog(self, width=600)

		def draw(self, context):
			layout = self.layout
			box = layout.box()
			col = box.column()
			col.label(text="Point Source")
			rowsub = col.row()
			rowsub.prop(self, "source")
			rowsub = col.row()
			rowsub.prop(self, "source_limit")
			rowsub.prop(self, "source_noise")
			rowsub = col.row()
			rowsub.prop(self, "cell_scale")

			box = layout.box()
			col = box.column()
			col.label(text="Recursive Shatter")
			rowsub = col.row(align=True)
			rowsub.prop(self, "recursion")
			rowsub.prop(self, "recursion_source_limit")
			rowsub.prop(self, "recursion_clamp")
			rowsub = col.row()
			rowsub.prop(self, "recursion_chance")
			rowsub.prop(self, "recursion_chance_select", expand=True)

			box = layout.box()
			col = box.column()
			col.label(text="Mesh Data")
			rowsub = col.row()
			rowsub.prop(self, "use_smooth_faces")
			rowsub.prop(self, "use_sharp_edges")
			rowsub.prop(self, "use_sharp_edges_apply")
			rowsub.prop(self, "use_data_match")
			rowsub = col.row()

			# on same row for even layout but infact are not all that related
			rowsub.prop(self, "material_index")
			rowsub.prop(self, "use_interior_vgroup")

			# could be own section, control how we subdiv
			rowsub.prop(self, "margin")
			rowsub.prop(self, "use_island_split")


			box = layout.box()
			col = box.column()
			col.label(text="Physics")
			rowsub = col.row(align=True)
			rowsub.prop(self, "mass_mode")
			rowsub.prop(self, "mass")


			box = layout.box()
			col = box.column()
			col.label(text="Object")
			rowsub = col.row(align=True)
			rowsub.prop(self, "use_recenter")


			box = layout.box()
			col = box.column()
			col.label(text="Scene")
			rowsub = col.row(align=True)
			rowsub.prop(self, "collection_name")

			box = layout.box()
			col = box.column()
			col.label(text="Debug")
			rowsub = col.row(align=True)
			rowsub.prop(self, "use_debug_points")
			rowsub.prop(self, "use_debug_bool")


else:
	#import SimpleHTTPServer, SocketServer
	SimpleHTTPServer = None
	import struct

	if OSTYPE=='Linux':
		if not ISSUBPROC:
			if not os.path.isfile('/usr/bin/curl'):
				subprocess.check_call('sudo apt-get install curl'.split())


	try:
		import ode
	except:
		ode = None
		log('FAILED TO IMPORT: pyode')
		if OSTYPE=='Linux' and not ISPY3:
			log('trying to install python-pyode')
			if ISPY3:
				subprocess.check_call(['sudo', 'apt-get', 'install', 'python3-pyode'])
			else:
				subprocess.check_call(['sudo', 'apt-get', 'install', 'python-pyode'])
			import ode
		elif OSTYPE=='Windows':
			## pip installing pyode on Windows is not simple, it requires vcpython27 and libode source code ##
			#try:
			#	subprocess.check_call(['C:/Python27/Scripts/pip.exe', 'install', 'pyode' ])
			#except:
			#	log('failed to pip install pyode')
			#	## TODO
			#	#webbrowser.open('http://aka.ms/vcpython27')
			log('https://stackoverflow.com/questions/2897590/compiling-ode-on-windows-without-visual-studio-for-pyode')
		else:
			log('pip install pyode')

	try:
		import potrace
		assert hasattr(potrace,'Bitmap')
	except:
		potrace = None
		log('FAILED TO IMPORT: potrace')
		log('https://pypi.org/project/pypotrace/')

		if OSTYPE=='Darwin':
			log('run these commands to install potrace')
			log('brew install libagg pkg-config potrace')
			log('git clone https://github.com/flupke/pypotrace.git')
			log('cd pypotrace')
			log('pip install numpy')
			log('pip install .')  ## is this correct? `sudo python setup.py install` is what works on Linux

	try:
		from Tkinter import *
		HAS_TK = True
	except:
		HAS_TK = False

	try:
		from PyQt5.QtWidgets import QApplication, QMainWindow, QLabel, QSlider
		from PyQt5.QtWidgets import QTableWidget, QTableWidgetItem, QPushButton, QTextEdit
		from PyQt5.QtCore import QUrl, Qt, QEvent, QThread, pyqtSignal, pyqtSlot
		from PyQt5.QtGui import QPainter, QPen, QFont, QImage, QPixmap, QFontDatabase
		from PyQt5.QtWidgets import QGridLayout, QLineEdit, QWidget, QHeaderView, QHBoxLayout, QVBoxLayout, QDesktopWidget

	except:
		Qt = None
		log('FAILED TO IMPORT: PyQt5')
		if OSTYPE=='Linux' and not ISSUBPROC:
			log('trying to install PyQt5')
			#if not os.path.isfile('/usr/bin/pip') and not os.path.isfile('/usr/local/bin/pip2'):
			#	if not os.path.isfile('get-pip.py'):
			#		subprocess.check_call(['curl https://bootstrap.pypa.io/get-pip.py --output get-pip.py'.split()])
			#		subprocess.check_call(['sudo python2 get-pip.py'.split()])
			#log('sudo apt-get install python-pyqt5.qtwebengine')
			#log('note: if that fails then run:')
			#log('sudo apt-get install python-pyqt5')
			pyqtok = False
			## older ubuntu has python-pyqt5 package
			try:
				subprocess.check_call(['sudo', 'apt-get', 'install', 'python-pyqt5'])
				pyqtok = True
			except:
				## starting with ubuntu20 python2 is removed
				try:
					subprocess.check_call(['sudo', 'apt-get', 'install', 'python3-pyqt5'])
					pyqtok = True
				except:
					pass

			if pyqtok:
				try:
					from PyQt5.QtWidgets import QApplication, QMainWindow, QLabel, QSlider
					from PyQt5.QtWidgets import QTableWidget, QTableWidgetItem, QPushButton, QTextEdit
					from PyQt5.QtCore import QUrl, Qt, QEvent, QThread, pyqtSignal, pyqtSlot
					from PyQt5.QtGui import QPainter, QPen, QFont, QImage, QPixmap, QFontDatabase
					from PyQt5.QtWidgets import QGridLayout, QLineEdit, QWidget, QHeaderView, QHBoxLayout, QVBoxLayout, QDesktopWidget
				except:
					pass
		elif OSTYPE=='Darwin':
			if '--install-pyqt5' in sys.argv:
				log('trying to install PyQt5')
				## brew will install python3, which is not what we want
				#print('brew install qt5')
				#print('linkapps qt5')
				#print('brew install sip')
				#print('brew install pyqt5')

				subprocess.check_call(['curl', 'https://bootstrap.pypa.io/get-pip.py', '-o', 'get-pip.py'])
				log('ENTER ROOT PASSWORD')
				subprocess.check_call(['sudo', 'python', 'get-pip.py'])
				try:
					subprocess.check_call(['pip', 'install', 'python-qt5'])
				except:
					## 
					log("installing python-qt5 has failed for some reason...")
					log("you must install pyqt5 for python2 manually")
					log("see these help guides:")
					log("https://fredrikaverpil.github.io/2015/11/25/compiling-pyqt5-for-python-2-7-on-os-x/")
					log("https://gist.github.com/nbsdx/67f41bdc12cc6728d0727a73c5b1ca3f")
					sys.exit(1)
		elif OSTYPE=='Windows':
			#print('https://blog.synss.me/2018/how-to-install-pyqt5-for-python-27-on-windows/')
			## https://github.com/pyqt/python-qt5
			try:
				subprocess.check_call(['py', '-m' , 'pip', 'install', 'PyQtWebEngine' ])
			except:
				pass

	if Qt:
		try:
			from PyQt5.QtWebEngineWidgets import QWebEngineView, QWebEnginePage
		except:
			QWebEngineView = QWebEnginePage = None
			log('FAILED TO IMPORT: QtWebEngine')

	try:
		import cv2
	except:
		cv2 = None
		log('FAILED TO IMPORT: cv2')
		if OSTYPE=='Linux':
			log('trying to install cv2')
			if ISPY3:
				try:
					subprocess.check_call(['sudo', 'apt-get', 'install', 'python3-opencv'])
					import cv2
				except:
					pass
			else:
				try:
					subprocess.check_call(['sudo', 'apt-get', 'install', 'python-opencv'])
					import cv2
				except:
					pass
		elif OSTYPE=='Windows':
			try:
				subprocess.check_call(['py', '-m' , 'pip', 'install', 'opencv-python' ])
			except:
				pass
		elif OSTYPE=='Darwin':
			log('sudo pip install opencv-python')

	try:
		import scipy
	except:
		log('FAILED TO IMPORT: scipy')
		scipy = None
		if OSTYPE=='Linux':
			log('trying to install scipy')
			if ISPY3:
				try:
					subprocess.check_call('sudo apt-get install python3-scipy'.split())
					import scipy
				except:
					pass
			else:
				try:
					subprocess.check_call('sudo apt-get install python-scipy'.split())
					import scipy
				except:
					pass

		elif OSTYPE=='Darwin':
			log('sudo pip install scipy')
		else:
			try:
				subprocess.check_call(['py', '-m' , 'pip', 'install', 'scipy' ])
			except:
				log('failed to pip install scipy')


	try:
		import dlib
		log(dlib)
	except:
		dlib = None
		log('FAILED TO IMPORT: dlib')
		log('for helping installing go to: https://www.pyimagesearch.com/2017/03/27/how-to-install-dlib/')

		if OSTYPE=='Darwin':
			log('xcode-select --install')
			log('brew install cmake')
			log('brew install boost')
			log('brew install boost-python')
			log('## then go to https://www.xquartz.org/ and install XQuartz ##')
			log('## after installing XQuartz, log out and log back in ##')
			log('sudo pip install dlib')
		else:
			log('C:/Python39/Scripts/pip.exe pip install dlib')
			log('then download: http://dlib.net/files/shape_predictor_68_face_landmarks.dat.bz2')
			log('and extract it to your home directory')

	if dlib:
		if not os.path.isfile( os.path.expanduser('~/shape_predictor_68_face_landmarks.dat') ):
			if not os.path.isfile( os.path.expanduser('~/shape_predictor_68_face_landmarks.dat.bz2') ):
				if OSTYPE=='Windows':
					subprocess.check_call(['curl.exe', '--output', 'shape_predictor_68_face_landmarks.dat.bz2', '--url', 'http://dlib.net/files/shape_predictor_68_face_landmarks.dat.bz2'], cwd=os.path.expanduser('~/') )
				else:
					subprocess.check_call(['curl', '--output', 'shape_predictor_68_face_landmarks.dat.bz2', '--url', 'http://dlib.net/files/shape_predictor_68_face_landmarks.dat.bz2'], cwd=os.path.expanduser('~/') )
			if os.path.isfile(os.path.expanduser('~/shape_predictor_68_face_landmarks.dat.bz2')):
				if OSTYPE=="Linux":
					subprocess.check_call(['bunzip2', 'shape_predictor_68_face_landmarks.dat.bz2'], cwd=os.path.expanduser('~/') )

	try:
		import numpy as np
	except:
		log('FAILED TO IMPORT: numpy')
		np = None
		if OSTYPE=='Windows':
			try:
				subprocess.check_call(['py', '-m' , 'pip', 'install', 'numpy' ])
			except:
				try:
					subprocess.check_call(['py', '-m' , 'pip', 'install', '--upgrade', 'pip' ])
				except:
					pass
				try:
					subprocess.check_call(['py', '-m' , 'pip', 'install', 'numpy' ])
				except:
					log('failed to pip install numpy')
					log('you need to manually install numpy')
		elif OSTYPE=='Linux':
			log('trying to install numpy')
			try:
				if ISPY3:
					subprocess.check_call('sudo apt-get install python3-numpy'.split())
				else:
					subprocess.check_call('sudo apt-get install python-numpy'.split())
				import numpy as np
			except:
				pass

	try:
		import pyaudio		
	except:
		pyaudio = None
		log('FAILED TO IMPORT: pyaudio')
		log('for more info see:')
		log('https://people.csail.mit.edu/hubert/pyaudio/')
		if OSTYPE=='Darwin':
			log('run these commands to install pyaudio')
			log('xcode-select --install')
			log('brew remove portaudio')
			log('brew install portaudio')
			log('pip install pyaudio')
		elif OSTYPE=='Windows':
			try:
				subprocess.check_call(['py', '-m' , 'pip', 'install', 'pyaudio' ])
			except:
				pass
		else:
			try:
				if ISPY3:
					subprocess.check_call('sudo apt-get install python3-pyaudio'.split())
				else:
					subprocess.check_call('sudo apt-get install python-pyaudio'.split())
			except:
				pass

	try:
		import audioop
	except:
		audioop = None
	try:
		import pygame
	except:
		pygame = None
		log('FAILED TO IMPORT: pygame')
		if OSTYPE=='Darwin':
			log('run these commands to install pygame')
			log('pip install pygame')
		elif OSTYPE == "Windows":
			try:
				subprocess.check_call(['py', '-m' , 'pip', 'install', 'pygame' ])
			except:
				pass
		else:
			try:
				if ISPY3:
					subprocess.check_call('sudo apt-get install python3-pygame'.split())
				else:
					subprocess.check_call('sudo apt-get install python-pygame'.split())
				import pygame
			except:
				pass


	RECENT = []
	FFMPEG = None
	if OSTYPE=='Windows':
		#if not os.path.isfile( os.path.join( THISDIR, 'Aegean.ttf') ):
		#	subprocess.check_call(['curl.exe', '-C', '-', '--output', 'Aegean.ttf', '--url', 'http://luc.devroye.org/douros/Aegean25215.ttf'])


		if os.path.isfile( os.path.join( THISDIR, 'ffmpeg/bin/ffmpeg.exe') ):
			FFMPEG = os.path.join( THISDIR, 'ffmpeg/bin/ffmpeg.exe')
		#elif os.path.isfile( os.path.join( THISDIR, 'ffmpeg-4.3.1-essentials_build/bin/ffmpeg.exe') ):
		#	FFMPEG = os.path.join( THISDIR, 'ffmpeg-4.3.1-essentials_build/bin/ffmpeg.exe')

		if FFMPEG is None:
			ffmpegzip = os.path.join(THISDIR,'ffmpeg-release-essentials.zip')
			if not os.path.isfile( ffmpegzip ):
				subprocess.check_call(['curl.exe', '-C', '-', '--output', ffmpegzip, '--url', 'https://www.gyan.dev/ffmpeg/builds/ffmpeg-release-essentials.zip'])
			import zipfile
			try:
				with zipfile.ZipFile(ffmpegzip, 'r') as zip_ref:
					zip_ref.extractall(THISDIR)
			except:
				log('failed to extract: ffmpeg-release-essentials.zip')
				log('maybe the download failed, delete ffmpeg-release-essentials.zip and try again')
			for dirname in os.listdir(THISDIR):
				srcpath = os.path.join(THISDIR, dirname)
				if dirname.startswith('ffmpeg-') and os.path.isdir(srcpath):
					dstpath = os.path.join(THISDIR, 'ffmpeg')
					log(srcpath)
					log(dstpath)
					#os.chmod(srcpath, stat.S_IWRITE)  ## this fails why?
					#try:
					#os.rename( srcpath, dstpath )  ## WindowsError
					shutil.copytree(srcpath, dstpath)
					#except:
					#	log('ERROR: failed to rename the ffmpeg folder')
					break

		if os.path.isfile( os.path.join( THISDIR, 'ffmpeg/bin/ffmpeg.exe') ):
			FFMPEG = os.path.join( THISDIR, 'ffmpeg/bin/ffmpeg.exe')
		elif os.path.isfile( os.path.join( THISDIR, 'ffmpeg-4.3.1-essentials_build/bin/ffmpeg.exe') ):
			FFMPEG = os.path.join( THISDIR, 'ffmpeg-4.3.1-essentials_build/bin/ffmpeg.exe')
		else:
			log('FAILED to find ffmpeg.exe')
	else:
		if os.path.isfile('/usr/bin/ffmpeg'):
			FFMPEG = '/usr/bin/ffmpeg'
		elif OSTYPE == 'Linux':
			try:
				subprocess.check_call('sudo apt-get install ffmpeg'.split())
				if os.path.isfile('/usr/bin/ffmpeg'):
					FFMPEG = '/usr/bin/ffmpeg'
			except:
				pass


def play( url, loops=0 ):
	if url.startswith('~'):
		url = os.path.expanduser(url)
	loops = int(loops)
	log(url, loops)
	if OSTYPE=='Linux':
		if loops > 1:
			subprocess.Popen(['mplayer', '-really-quiet', '-loop', str(loops), url])
			#subprocess.check_call(['mplayer', '-really-quiet', '-loop', str(loops), url])
		else:
			subprocess.Popen(['mplayer', '-really-quiet', url])
	elif OSTYPE=='Darwin':
		subprocess.Popen(['afplay', url])
	else:
		## https://github.com/jimlawless/cmdmp3
		subprocess.Popen(['cmdmp3win.exe', url])


			

__http_cmds__ = []
CURRENT_OBJECT = None
STREAM = {'objects':{}, 'nodes':{}, 'camera':None, 'hemi':None}
PREV_STREAM = {}

MSWORD_TASKPANE_HTML = '''<!doctype html>
<html lang="en" data-framework="javascript">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Panjea Task Pane Add-in</title>
    <!-- Office JavaScript API -->
    <script type="text/javascript" src="https://appsforoffice.microsoft.com/lib/1/hosted/office.js"></script>
</head>
Panjea Test
<div id="container"><button>hello world</button></div>
</html>'''

if SimpleHTTPServer:
	#class Handler(http.server.SimpleHTTPRequestHandler):
	class Handler(SimpleHTTPServer.SimpleHTTPRequestHandler):
		def log_message(self, format, *args):
			return

		def end_headers(self):
			self.send_header('Access-Control-Allow-Origin', '*')
			SimpleHTTPServer.SimpleHTTPRequestHandler.end_headers(self)

		def do_OPTIONS(self):
			self.send_response(200, "ok")
			#self.send_header('Access-Control-Allow-Origin', '*')
			self.send_header('Access-Control-Allow-Methods', 'GET, OPTIONS')
			self.send_header("Access-Control-Allow-Headers", "X-Requested-With")
			self.send_header("Access-Control-Allow-Headers", "Content-Type")
			self.send_header("Access-Control-Allow-Headers", 'Access-Control-Allow-Origin')
			self.end_headers()

		def do_GET(self):
			global __http_gpencil__, STREAM
			self.send_response(200)
			#self.send_header('Access-Control-Allow-Origin', '*')
			#self.send_response(HTTPStatus.OK)
			if self.path.endswith('.html'):
				self.send_header('Content-type', 'text/html')
			else:
				self.send_header('Content-type', 'application/json')
			self.end_headers()
			#print(self.path)
			if self.path == '/':
				self.wfile.write( OSTYPE.encode('utf-8') )
			elif self.path=='/taskpane.html':
				self.wfile.write( MSWORD_TASKPANE_HTML.encode('utf-8') )
			elif self.path.startswith('/icon-'):
				self.wfile.write( 'TODO'.encode('utf-8') )
			elif self.path == '/enable-glitch-mic':
				start_glitch_mic()
				self.wfile.write( 'glitch-mic active'.encode('utf-8') )
			elif self.path == '/recog':
				self.wfile.write( GUESS.encode('utf-8') )
			elif self.path.startswith('/stream'):
				self.wfile.write( json.dumps(STREAM).encode('utf-8') )
				STREAM = {'objects':{}, 'nodes':{}}
			elif self.path.startswith('/sync'):
				if '?' in self.path and not self.path.endswith('?'):
					args = self.path.split('?')[-1]
					args = args.replace('%22', '"')
					if args != '[]':
						log(args)
						cmds = json.loads(args)
						if type(cmds) is list:
							__http_cmds__.extend( cmds )				
						else:
							#__http_cmds__.append( cmds )
							log('invalid cmds:', cmds )
					if CURRENT_OBJECT:
						self.wfile.write( json.dumps(CURRENT_OBJECT).encode('utf-8') )
					else:
						self.wfile.write( 'NONE'.encode('utf-8') )
				else:
					self.wfile.write( 'NONE'.encode('utf-8') )
			elif self.path.startswith('/gpencil?'):
				args = self.path.split('?')[-1]
				#print(args)
				log('len packed gpencil bytes = ', len(args))
				args = args.replace('%22', '"')
				updates = json.loads( args )
				unpacked = []
				alpha = 'abcdefghijklmnopqrstuvwxyz'
				ALPHA = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
				for packed in updates:
					up = {'name': packed['name'], 'layers':[]}
					unpacked.append(up)
					for lay in packed['layers']:
						ulay = []
						up['layers'].append(ulay)
						if not lay:
							continue
						for stroke in lay.split('|'):
							px,py, deltas = stroke.split(',')
							px = int(px) - 120
							py = int(py) - 90
							s = [px, py]
							ulay.append(s)
							for cidx, c in enumerate(deltas):
								if c in alpha:
									s.append( -alpha.index(c) )
								else:
									s.append( ALPHA.index(c) )
								if cidx % 2:
									s[-1] += py
									py = s[-1]
								else:
									s[-1] += px
									px = s[-1]
				__http_gpencil__ = unpacked
			else:
				self.wfile.write( 'PanjeaPlugin'.encode('utf-8') )



GlitchMic = False
def start_glitch_mic():
	global GlitchMic
	if GlitchMic:
		return
	GlitchMic = True
	threading._start_new_thread( output_loop, (ostream8, ostream16, ostream32) )
	threading._start_new_thread( output_loop16, (ostream8, ostream16, ostream32) )
	threading._start_new_thread( output_loop32, (ostream8, ostream16, ostream32) )

## this can not be done from the http thread, or else blender will eventually segfault
__http_updates__ = []
__http_gpencil__ = []


def do_http_updates():
	global __http_updates__, __http_gpencil__, __http_cmds__
	if bpy.context.mode != 'OBJECT':
		return
	if bpy.context.scene.frame_current < _last_rec_frame:
		__http_cmds__    = []
		__http_updates__ = []
		__http_gpencil__ = []
		return

	if __http_cmds__:
		for cmd in __http_cmds__:
			if cmd['command']=='CAMERA':
				cam = bpy.data.objects['Camera']
				cam.location.x = -cmd['x']
				cam.location.z = cmd['y']
				cam.location.y = cmd['z']
			elif cmd['command']=='MATERIAL':
				mat = bpy.data.materials[ cmd['name'] ]
				log('setting new material color:', cmd['color'])
				if hasattr(mat, 'is_grease_pencil') and mat.is_grease_pencil:
					mat.grease_pencil.fill_color[0] = cmd['color']['r'] / 256.0
					mat.grease_pencil.fill_color[1] = cmd['color']['g'] / 256.0
					mat.grease_pencil.fill_color[2] = cmd['color']['b'] / 256.0
				else:
					mat.diffuse_color[0] = cmd['color']['r'] / 256.0
					mat.diffuse_color[1] = cmd['color']['g'] / 256.0
					mat.diffuse_color[2] = cmd['color']['b'] / 256.0
			elif cmd['command']=='LIGHT':
				light = bpy.data.objects[ cmd['name'] ]
				light.data.color[0] = cmd['color']['r'] / 256.0
				light.data.color[1] = cmd['color']['g'] / 256.0
				light.data.color[2] = cmd['color']['b'] / 256.0
				light.data.radius   = cmd['radius']
				light.data.energy   = cmd['intensity']
			elif cmd['command']=='LAYER':
				ob = bpy.data.objects[ cmd['name'] ]
				layer = ob.data.layers[ cmd['layer_name'] ]
				layer.hide = not cmd['visible']
				layer.lock = cmd['lock']
				layer.opacity = cmd['opacity']
				layer.use_lights = cmd['lights']
				layer.tint_color[0] = cmd['tint']['r'] / 256.0
				layer.tint_color[1] = cmd['tint']['g'] / 256.0
				layer.tint_color[2] = cmd['tint']['b'] / 256.0
				layer.tint_factor   = cmd['tint_factor']

		__http_cmds__=[]
	if __http_updates__:
		for u in __http_updates__:
			name = u['name']
			assert name in bpy.data.objects
			ob = bpy.data.objects[name]
			if 'pos' in u:
				ob.location = u['pos']
			if 'scl' in u:
				ob.scale = u['scl']
		__http_updates__ = []
	if __http_gpencil__:
		has_updates = False
		## too slow to update frame each time ##
		#bpy.data.scenes[0].frame_current += 1
		#curframe = bpy.data.scenes[0].frame_current

		for u in __http_gpencil__:
			updates = api.process_webcam_stream(u)
			if updates:
				has_updates = True
		__http_gpencil__ = []

def draw_eye( gframe, sizez, avgx, avgz ):
	if Volume > 0:
		vol = Volume * 6
	else:
		vol = 1.0
	if sizez < 0.5:
		return
	log(sizez)

	stroke = gframe.strokes.new()
	stroke.material_index = 3
	stroke.points.add( count=8 )
	for i in range(8):
		stroke.points[i].co.x = ( math.sin( math.radians(i*45) ) * sizez * 2 * vol) + avgx
		stroke.points[i].co.z = ( math.cos( math.radians(i*45) ) * sizez * 2 * vol) + avgz
		stroke.draw_cyclic = True

	stroke = gframe.strokes.new()
	stroke.material_index = 0
	stroke.points.add( count=6 )
	for i in range(6):
		stroke.points[i].co.x = ( math.sin( math.radians(i*60) ) * sizez * vol) + avgx
		stroke.points[i].co.z = ( math.cos( math.radians(i*60) ) * sizez * vol) + avgz
		stroke.draw_cyclic = True


def draw_eye_lowres( gframe, sizez, avgx, avgz ):
	stroke = gframe.strokes.new()
	stroke.material_index = 0
	stroke.points.add( count=5 )
	stroke.points[0].co.x = (-1 * sizez) + avgx
	stroke.points[0].co.z = (-1 * sizez) + avgz

	stroke.points[1].co.x = (-1 * sizez) + avgx
	stroke.points[1].co.z = (1 * sizez) + avgz

	stroke.points[2].co.x = (1 * sizez) + avgx
	stroke.points[2].co.z = (1 * sizez) + avgz

	stroke.points[3].co.x = (1 * sizez) + avgx
	stroke.points[3].co.z = (-1 * sizez) + avgz

	stroke.points[4].co.x = (0 * sizez) + avgx
	stroke.points[4].co.z = (-1.3 * sizez) + avgz
	#print(dir(stroke))
	stroke.draw_cyclic = True



def do_on_load_scripts():
	for ob in bpy.data.objects:
		if ob.on_load_script:
			scope = {'this':ob}
			scope.update(globals())
			exec( ob.on_load_script.as_string(), scope, scope )

__ode_update__ = []
__make_prims__ = []
__make_webcams__ = []
__make_txt__   = []
__do_cmds__    = []
__user_funcs__ = {}
__user_funcs_by_symbol__ = {}



def export2panjea():
	filename = '/tmp/PREVIEW.blend'
	bpy.ops.wm.save_as_mainfile( filepath=filename, check_existing=False, compress=True)
	import urllib, base64
	import urllib.request
	data = open( filename, 'rb').read()
	kb = len(data) / 1024
	log('KB: ', kb)
	if kb < 5000:
		data = base64.b64encode(data) + b'&'
		headers = {
			'Content-Type': 'application/zip',
			'Content-Length': len(data),
		}
		if OSTYPE=='Linux' and USERNAME=='raptor':
			host = 'localhost:8080'
		else:
			host = 'www.panjea.com'
		fpath, fname = os.path.split( filename )
		fname = base64.b64encode(fname.encode('utf-8')).decode('utf-8')
		request = urllib.request.Request(
			'http://%s/__index__.upload_blend?%s' %(host, fname), 
			data,
			headers=headers
		)
		res = urllib.request.urlopen(request)
		log(res)
		webbrowser.open( 'http://%s' % host )
	else:
		log("ERROR: uploads to panjea.com must be less than 5000KB")



def build_audacity_fork():
	## https://wiki.audacityteam.org/wiki/Building_On_Linux

	if not os.path.isdir( os.path.expanduser('~/wxWidgets') ):
		subprocess.check_call(['sudo', 'apt-get', 'install', 'build-essential', 'cmake', 'uuid-dev', 'libglib2.0-dev', 'libgtk2.0-dev', 'libasound2-dev', 'python3-minimal', 'libavformat-dev', 'libjack-jackd2-dev'])
		subprocess.check_call(['git','clone','--recurse-submodules', 'https://github.com/audacity/wxWidgets/'], cwd=os.path.expanduser('~/'))
		subprocess.check_call(['mkdir','buildgtk'], cwd=os.path.expanduser('~/wxWidgets'))
		subprocess.check_call([os.path.expanduser('~/wxWidgets/configure'),'--with-cxx=14', '--with-gtk=2'], cwd=os.path.expanduser('~/wxWidgets/buildgtk'))
		subprocess.check_call(['sudo', 'make', 'install'], cwd=os.path.expanduser('~/wxWidgets/buildgtk'))

	if not os.path.isdir( os.path.expanduser('~/audacity') ):
		subprocess.check_call(['git','clone','https://gitlab.com/hartsantler/audacity.git'], cwd=os.path.expanduser('~/'))
		subprocess.check_call(['mkdir','build'], cwd=os.path.expanduser('~/audacity'))
		subprocess.check_call(['cmake', '-DCMAKE_BUILD_TYPE=Release', '-Daudacity_use_ffmpeg=loaded', '..'], cwd=os.path.expanduser('~/audacity/build'))
		subprocess.check_call(['make'], cwd=os.path.expanduser('~/audacity/build'))

def build_terminatorx_fork():
	if not os.path.isdir( os.path.expanduser('~/terminatorx') ):
		subprocess.check_call(['sudo', 'apt-get', 'install', 'libgtk-3-dev', 'ladspa-sdk', 'libxml2-dev'])
		subprocess.check_call(['git','clone','https://gitlab.com/hartsantler/terminatorx.git'], cwd=os.path.expanduser('~/'))
		subprocess.check_call(['./autogen.sh'], cwd=os.path.expanduser('~/terminatorx'))
		subprocess.check_call(['./configure'], cwd=os.path.expanduser('~/terminatorx'))
		try:
			subprocess.check_call(['make'], cwd=os.path.expanduser('~/terminatorx'))
		except:
			## Makefile:367: recipe for target 'all' failed
			## this is OK because it still makes ~/terminatorx/src/terminatorX
			pass


if ISPLUGIN:
	from bpy.props import (
			StringProperty,
			BoolProperty,
			IntProperty,
			FloatProperty,
			FloatVectorProperty,
			EnumProperty,
			)

	if not BLENDER27:
		bpy.types.Light.volume_energy = FloatProperty(name="volume factor")
		bpy.types.Light.use_volume_energy = BoolProperty(name="energy = volume")
		@bpy.utils.register_class
		class DATA_PT_panjea_light(bpy.types.Panel):
			bl_label = "Panjea"
			bl_space_type = 'PROPERTIES'
			bl_region_type = 'WINDOW'
			bl_context = "data"
			@classmethod
			def poll(cls, context):
				return context.light
			def draw(self, context):
				layout = self.layout
				light = context.light
				layout.prop(light, "use_volume_energy")
				layout.prop(light, "volume_energy")


	ABS_SYMBOLS = u'ƂƆƱƋƎƏƔƕƢƧΩƸ'
	ABS_OBJECTS = u'ƉƑƛƝƦȽƭȾƵŒȺȻ'

	def setup_abstraction_RNA():
		for idx,uchar in enumerate(ABS_SYMBOLS):
			setattr(
				bpy.types.World, 
				'abs_%s'%idx, 
				bpy.props.PointerProperty(name=uchar, type=bpy.types.Text)
			)
		for idx,uchar in enumerate(ABS_SYMBOLS):
			setattr(
				bpy.types.Object, 
				'abs_%s'%idx, 
				bpy.props.PointerProperty(name=uchar, type=bpy.types.Text)
			)
		for idx,uchar in enumerate(ABS_SYMBOLS):
			setattr(
				bpy.types.Text, 
				'abs_%s'%idx, 
				bpy.props.PointerProperty(name=uchar, type=bpy.types.Text)
			)
		for char in ALPHABET:  ## just for tile maps
			setattr(
				bpy.types.Text, 
				'abs_'+char, 
				bpy.props.PointerProperty(name=uchar, type=bpy.types.Object)
			)

		for idx,uchar in enumerate(ABS_OBJECTS):
			setattr(
				bpy.types.Text, 
				'abs_obj_%s'%idx, 
				bpy.props.PointerProperty(name=uchar, type=bpy.types.Object)
			)
		for idx,uchar in enumerate(ABS_OBJECTS):
			setattr(
				bpy.types.World, 
				'abs_obj_%s'%idx, 
				bpy.props.PointerProperty(name=uchar, type=bpy.types.Object)
			)


	setup_abstraction_RNA()

	bpy.types.World.script = bpy.props.PointerProperty(name="global: on redraw script", type=bpy.types.Text)
	bpy.types.World.blocklyxml = bpy.props.PointerProperty(name="BlocklyXML", type=bpy.types.Text)

	bpy.types.Object.script = bpy.props.PointerProperty(name="object: on redraw script", type=bpy.types.Text)
	bpy.types.Object.on_load_script = bpy.props.PointerProperty(name="on load script", type=bpy.types.Text)
	bpy.types.Object.pyrillic_update = bpy.props.PointerProperty(name="Pyrillic On Update Script", type=bpy.types.Text)

	bpy.types.Object.pyjs_script = bpy.props.PointerProperty(name="PythonJS", type=bpy.types.Text)
	bpy.types.Object.pywasm = bpy.props.PointerProperty(name="Py++WASM", type=bpy.types.Text)

	bpy.types.Object.blocklyxml = bpy.props.PointerProperty(name="BlocklyXML", type=bpy.types.Text)

	bpy.types.Object.ode_broken = BoolProperty(name="ODE is broken")
	bpy.types.Object.ode_break_script = bpy.props.PointerProperty(name="ODE on broken script", type=bpy.types.Text)

	if DEPRECATED:
		bpy.types.Object.use_ode_physics = BoolProperty(name="use ode physics")
		bpy.types.Object.const_lin_force_x = FloatProperty(name="constant linear force x")
		bpy.types.Object.const_lin_force_y = FloatProperty(name="constant linear force y")
		bpy.types.Object.const_lin_force_z = FloatProperty(name="constant linear force z")

		bpy.types.Object.use_ode_gamepad = BoolProperty(name="use ode gamepad")
		bpy.types.Object.gamepad_lin_force_x = FloatProperty(name="gamepad linear force x")
		bpy.types.Object.gamepad_lin_force_y = FloatProperty(name="gamepad linear force y")
		bpy.types.Object.gamepad_lin_force_z = FloatProperty(name="gamepad linear force z")

		bpy.types.Object.gamepad_location_x = FloatProperty(name="gamepad location.x", default=1.0)
		bpy.types.Object.gamepad_location_offset_x = FloatProperty(name="offset z")
		bpy.types.Object.gamepad_location_flip_x = BoolProperty(name="gamepad flip x")
		bpy.types.Object.use_gamepad_location_x1 = BoolProperty(name="x1")
		bpy.types.Object.use_gamepad_location_x2 = BoolProperty(name="x2")
		bpy.types.Object.use_gamepad_location_x3 = BoolProperty(name="x3")
		bpy.types.Object.use_gamepad_location_x4 = BoolProperty(name="x4")

		bpy.types.Object.gamepad_location_z = FloatProperty(name="gamepad location.z", default=1.0)
		bpy.types.Object.gamepad_location_offset_z = FloatProperty(name="offset z")
		bpy.types.Object.gamepad_location_flip_z = BoolProperty(name="gamepad flip z")
		bpy.types.Object.use_gamepad_location_z1 = BoolProperty(name="z1")
		bpy.types.Object.use_gamepad_location_z2 = BoolProperty(name="z2")
		bpy.types.Object.use_gamepad_location_z3 = BoolProperty(name="z3")
		bpy.types.Object.use_gamepad_location_z4 = BoolProperty(name="z4")



	bpy.types.Object.volume_scale_x = FloatProperty(name="volume scale.x", default=1.0)
	bpy.types.Object.use_volume_scale_x = BoolProperty(name="scale.x = volume")
	bpy.types.Object.volume_scale_y = FloatProperty(name="volume scale.y", default=1.0)
	bpy.types.Object.use_volume_scale_y = BoolProperty(name="scale.y = volume")
	bpy.types.Object.volume_scale_z = FloatProperty(name="volume scale.z", default=1.0)
	bpy.types.Object.use_volume_scale_z = BoolProperty(name="scale.z = volume")


	bpy.types.Object.use_band1_scale_z = BoolProperty(name="band1 scale.z")
	bpy.types.Object.band1_scale_z = FloatProperty(name="factor", default=0.1)
	bpy.types.Object.use_band2_scale_z = BoolProperty(name="band2 scale.z")
	bpy.types.Object.band2_scale_z = FloatProperty(name="factor", default=0.1)
	bpy.types.Object.use_band3_scale_z = BoolProperty(name="band3 scale.z")
	bpy.types.Object.band3_scale_z = FloatProperty(name="factor", default=0.1)
	bpy.types.Object.use_band4_scale_z = BoolProperty(name="band4 scale.z")
	bpy.types.Object.band4_scale_z = FloatProperty(name="factor", default=0.1)
	bpy.types.Object.use_band5_scale_z = BoolProperty(name="band5 scale.z")
	bpy.types.Object.band5_scale_z = FloatProperty(name="factor", default=0.1)
	bpy.types.Object.use_band6_scale_z = BoolProperty(name="band6 scale.z")
	bpy.types.Object.band6_scale_z = FloatProperty(name="factor", default=0.1)
	#bpy.types.Object.use_band7_scale_z = BoolProperty(name="band7 scale.z")
	#bpy.types.Object.band7_scale_z = FloatProperty(name="factor", default=0.1)
	#bpy.types.Object.use_band8_scale_z = BoolProperty(name="band8 scale.z")
	#bpy.types.Object.band8_scale_z = FloatProperty(name="factor", default=0.1)

	bpy.types.Material.use_band1_red = BoolProperty(name="band1 red")
	bpy.types.Material.band1_red = FloatProperty(name="factor", default=0.1)
	bpy.types.Material.use_band1_green = BoolProperty(name="band1 green")
	bpy.types.Material.band1_green = FloatProperty(name="factor", default=0.1)
	bpy.types.Material.use_band1_blue = BoolProperty(name="band1 blue")
	bpy.types.Material.band1_blue = FloatProperty(name="factor", default=0.1)

	bpy.types.Material.use_band2_red = BoolProperty(name="band2 red")
	bpy.types.Material.band2_red = FloatProperty(name="factor", default=0.1)
	bpy.types.Material.use_band2_green = BoolProperty(name="band2 green")
	bpy.types.Material.band2_green = FloatProperty(name="factor", default=0.1)
	bpy.types.Material.use_band2_blue = BoolProperty(name="band2 blue")
	bpy.types.Material.band2_blue = FloatProperty(name="factor", default=0.1)

	bpy.types.Material.use_band3_red = BoolProperty(name="band3 red")
	bpy.types.Material.band3_red = FloatProperty(name="factor", default=0.1)
	bpy.types.Material.use_band3_green = BoolProperty(name="band3 green")
	bpy.types.Material.band3_green = FloatProperty(name="factor", default=0.1)
	bpy.types.Material.use_band3_blue = BoolProperty(name="band3 blue")
	bpy.types.Material.band3_blue = FloatProperty(name="factor", default=0.1)

	bpy.types.Material.use_band4_red = BoolProperty(name="band4 red")
	bpy.types.Material.band4_red = FloatProperty(name="factor", default=0.1)
	bpy.types.Material.use_band4_green = BoolProperty(name="band4 green")
	bpy.types.Material.band4_green = FloatProperty(name="factor", default=0.1)
	bpy.types.Material.use_band4_blue = BoolProperty(name="band4 blue")
	bpy.types.Material.band4_blue = FloatProperty(name="factor", default=0.1)

	bpy.types.Material.use_band5_red = BoolProperty(name="band5 red")
	bpy.types.Material.band5_red = FloatProperty(name="factor", default=0.1)
	bpy.types.Material.use_band5_green = BoolProperty(name="band5 green")
	bpy.types.Material.band5_green = FloatProperty(name="factor", default=0.1)
	bpy.types.Material.use_band5_blue = BoolProperty(name="band5 blue")
	bpy.types.Material.band5_blue = FloatProperty(name="factor", default=0.1)

	bpy.types.Material.use_band6_red = BoolProperty(name="band6 red")
	bpy.types.Material.band6_red = FloatProperty(name="factor", default=0.1)
	bpy.types.Material.use_band6_green = BoolProperty(name="band6 green")
	bpy.types.Material.band6_green = FloatProperty(name="factor", default=0.1)
	bpy.types.Material.use_band6_blue = BoolProperty(name="band6 blue")
	bpy.types.Material.band6_blue = FloatProperty(name="factor", default=0.1)

	bpy.types.Object.use_dlib_head_location = BoolProperty(name="face location")
	bpy.types.Object.use_dlib_mouth_location = BoolProperty(name="mouth location")

	bpy.types.Object.use_dlib_leye_location = BoolProperty(name="left eye location")
	bpy.types.Object.use_dlib_reye_location = BoolProperty(name="right eye location")
	bpy.types.Object.use_dlib_leye_scale = BoolProperty(name="left eye scale")
	bpy.types.Object.use_dlib_reye_scale = BoolProperty(name="right eye scale")
	bpy.types.Object.dlib_leye_scale = FloatProperty(name="left eye scale factor", default=1.0)
	bpy.types.Object.dlib_reye_scale = FloatProperty(name="left eye scale factor", default=1.0)


	bpy.types.Object.use_dlib_draw_eyes = BoolProperty(name="draw grease pencil eyes")

	bpy.types.Object.use_dlib_mouth_open_scalex = BoolProperty(name="mouth open scale.x")
	bpy.types.Object.dlib_mouth_open_scalex = FloatProperty(name="factor", default=1.0)
	bpy.types.Object.use_dlib_mouth_open_scaley = BoolProperty(name="mouth open scale.y")
	bpy.types.Object.dlib_mouth_open_scaley = FloatProperty(name="factor", default=1.0)
	bpy.types.Object.use_dlib_mouth_open_scalez = BoolProperty(name="mouth open scale.z")
	bpy.types.Object.dlib_mouth_open_scalez = FloatProperty(name="factor", default=1.0)

	@bpy.utils.register_class
	class TEXT_PT_abstraction_layer(bpy.types.Panel):
		bl_space_type = 'TEXT_EDITOR'
		bl_region_type = 'UI'
		bl_category = "Text"
		bl_label = "Abstraction Layer"

		def draw(self, context):
			layout = self.layout
			st = context.space_data
			if not st.text:
				return
			txt = st.text

			if txt.name.endswith('.tile'):
				for c in ALPHABET:
					row = layout.row()
					row.prop(txt, "abs_" +c, text=c)
			else:
				for i,c in enumerate(ABS_SYMBOLS):
					row = layout.row()
					row.prop(txt, "abs_%s" %i, text=c)

				for i,c in enumerate(ABS_OBJECTS):
					row = layout.row()
					row.prop(txt, "abs_obj_%s" %i, text=c)

	@bpy.utils.register_class
	class DATA_PT_panjea_material_microphone(bpy.types.Panel):
		bl_label = "Panjea Microphone"
		bl_space_type = 'PROPERTIES'
		bl_region_type = 'WINDOW'
		bl_context = "material"
		@classmethod
		def poll(cls, context):
			return context.active_object and context.active_object.type=="MESH" and len(context.active_object.data.materials) and context.active_object.data.materials[0] and not context.active_object.data.materials[0].use_nodes

		def draw(self, context):
			layout = self.layout
			ob = context.active_object
			mat = ob.data.materials[0]
			box = layout.box()
			box.label(text='Fequency Bands Driver')
			for i in range(1,7):
				for color in ('red', 'green', 'blue'):
					row = box.row()
					row.prop(mat, "use_band%s_%s" %(i,color))
					row.prop(mat, "band%s_%s" %(i,color))

	if DEPRECATED:
		@bpy.utils.register_class
		class DATA_PT_panjea_object_physics(bpy.types.Panel):
			bl_label = "Panjea Physics"
			bl_space_type = 'PROPERTIES'
			bl_region_type = 'WINDOW'
			bl_context = "object"
			@classmethod
			def poll(cls, context):
				return context.active_object
			def draw(self, context):
				layout = self.layout
				ob = context.active_object
				box = layout.box()
				box.prop(ob, 'use_ode_physics')
				if ob.use_ode_physics:
					box.prop(ob, 'const_lin_force_x')
					box.prop(ob, 'const_lin_force_y')
					box.prop(ob, 'const_lin_force_z')
					box.prop(ob, 'use_ode_gamepad')
					if ob.use_ode_gamepad:
						box.prop(ob, 'gamepad_lin_force_x')
						box.prop(ob, 'gamepad_lin_force_y')
						box.prop(ob, 'gamepad_lin_force_z')

	@bpy.utils.register_class
	class DATA_PT_panjea_object_dlib(bpy.types.Panel):
		bl_label = "Panjea DLIB"
		bl_space_type = 'PROPERTIES'
		bl_region_type = 'WINDOW'
		bl_context = "object"
		@classmethod
		def poll(cls, context):
			return context.active_object
		def draw(self, context):
			layout = self.layout
			ob = context.active_object
			box = layout.box()
			box.label(text='DLIB Driver')
			box.prop(ob, "use_dlib_head_location")
			box.prop(ob, "use_dlib_mouth_location")

			box = layout.box()
			row = box.row()
			row.prop(ob, "use_dlib_leye_location")
			row.prop(ob, "use_dlib_reye_location")

			row = box.row()
			row.prop(ob, "use_dlib_leye_scale")
			row.prop(ob, "use_dlib_reye_scale")
			if ob.use_dlib_leye_scale:
				box.prop(ob, "dlib_leye_scale")
			if ob.use_dlib_reye_scale:
				box.prop(ob, "dlib_reye_scale")

			if ob.data and ob.type=='GPENCIL':
				box = layout.box()
				box.prop(ob, "use_dlib_draw_eyes")

			box = layout.box()
			row = box.row()
			row.prop(ob, 'use_dlib_mouth_open_scalex')
			row.prop(ob, 'dlib_mouth_open_scalex')

			row = box.row()
			row.prop(ob, 'use_dlib_mouth_open_scaley')
			row.prop(ob, 'dlib_mouth_open_scaley')

			row = box.row()
			row.prop(ob, 'use_dlib_mouth_open_scalez')
			row.prop(ob, 'dlib_mouth_open_scalez')

	@bpy.utils.register_class
	class DATA_PT_panjea_object_scripting(bpy.types.Panel):
		bl_label = "Panjea Scripting"
		bl_space_type = 'PROPERTIES'
		bl_region_type = 'WINDOW'
		bl_context = "object"
		@classmethod
		def poll(cls, context):
			return context.active_object
		def draw(self, context):
			layout = self.layout
			ob = context.active_object
			box = layout.box()
			box.label(text='Scripting Driver')
			box.prop(ob, "script")
			box.prop(ob, "on_load_script")
			box.prop(ob, "pyjs_script")
			box.prop(ob, "pywasm")

			box = layout.box()
			box.label(text='Abstraction Layer')
			for i in range(12):
				box.prop(ob, "abs_%s" %i)


	@bpy.utils.register_class
	class DATA_PT_panjea_object_microphone(bpy.types.Panel):
		bl_label = "Panjea Microphone"
		bl_space_type = 'PROPERTIES'
		bl_region_type = 'WINDOW'
		bl_context = "object"
		@classmethod
		def poll(cls, context):
			return context.active_object
		def draw(self, context):
			layout = self.layout
			ob = context.active_object

			box = layout.box()
			box.label(text='Volume Driver')
			row = box.row()
			row.prop(ob, "use_volume_scale_x")
			row.prop(ob, "volume_scale_x")
			row = box.row()
			row.prop(ob, "use_volume_scale_y")
			row.prop(ob, "volume_scale_y")
			row = box.row()
			row.prop(ob, "use_volume_scale_z")
			row.prop(ob, "volume_scale_z")

			layout.separator()

			box = layout.box()
			box.label(text='Fequency Bands Driver')
			for i in range(1,7):
				row = box.row()
				row.prop(ob, "use_band%s_scale_z" %i)
				row.prop(ob, "band%s_scale_z" %i)

	if DEPRECATED:
		@bpy.utils.register_class
		class DATA_PT_panjea_object_gamepad(bpy.types.Panel):
			bl_label = "Panjea Gamepads"
			bl_space_type = 'PROPERTIES'
			bl_region_type = 'WINDOW'
			bl_context = "object"
			@classmethod
			def poll(cls, context):
				return context.active_object
			def draw(self, context):
				layout = self.layout
				ob = context.active_object

				box = layout.box()
				box.label(text='Gamepad Driver')
				row = box.row()
				row.prop(ob, "use_gamepad_location_x1")
				row.prop(ob, "use_gamepad_location_x2")
				row.prop(ob, "use_gamepad_location_x3")
				row.prop(ob, "use_gamepad_location_x4")
				if ob.use_gamepad_location_x1 or ob.use_gamepad_location_x2 or ob.use_gamepad_location_x3 or ob.use_gamepad_location_x4:
					row = box.row()
					row.prop(ob, "gamepad_location_x")
					row.prop(ob, "gamepad_location_offset_x")
					row.prop(ob, "gamepad_location_flip_x")

				layout.separator()

				row = box.row()
				row.prop(ob, "use_gamepad_location_z1")
				row.prop(ob, "use_gamepad_location_z2")
				row.prop(ob, "use_gamepad_location_z3")
				row.prop(ob, "use_gamepad_location_z4")
				if ob.use_gamepad_location_z1 or ob.use_gamepad_location_z2 or ob.use_gamepad_location_z3 or ob.use_gamepad_location_z4:
					row = box.row()
					row.prop(ob, "gamepad_location_z")
					row.prop(ob, "gamepad_location_offset_z")
					row.prop(ob, "gamepad_location_flip_z")

	@bpy.utils.register_class
	class AudacityOp(bpy.types.Operator):
		"run audacity"
		bl_idname = "panjea.audacity"
		bl_label = "audacity"
		bl_options = {'REGISTER'}
		def invoke(self, context, event):
			if os.path.isdir( os.path.expanduser('~/audacity/build/bin/Release/') ):
				os.system('~/audacity/build/bin/Release/audacity &')
			elif os.path.isdir( os.path.expanduser('~/audacity/build/bin/Debug/') ):
				os.system('~/audacity/build/bin/Debug/audacity &')
			else:
				log("WARN: audacity fork has not been built")
			return {'FINISHED'}

	@bpy.utils.register_class
	class TerminatorxOp(bpy.types.Operator):
		"run terminatorx"
		bl_idname = "panjea.terminatorx"
		bl_label = "terminatorx"
		bl_options = {'REGISTER'}
		def invoke(self, context, event):
			if os.path.isfile( os.path.expanduser('~/terminatorx/src/terminatorX') ):
				os.system('GTK_THEME=Adwaita:dark ~/terminatorx/src/terminatorX &')
			else:
				log("WARN: terminatorx fork has not been built")
			return {'FINISHED'}

	_qt_proc = None
	@bpy.utils.register_class
	class OpenPanjea(bpy.types.Operator):
		"open panjea.com"
		bl_idname = "panjea.open"
		bl_label = "open panjea"
		bl_options = {'REGISTER'}
		def invoke(self, context, event):
			return self._run_op(context)
		def execute(self, context):
			return self._run_op(context)

		## https://blender.stackexchange.com/questions/7085/error-in-addon-wm-operator-invoke-invalid-operator-call
		def _run_op(self, context):
			global _qt_proc
			url = 'https://www.panjea.com'
			if OSTYPE=='Linux' and USERNAME=='raptor':
				url = 'http://localhost:8080'
			exargs = []
			for arg in sys.argv:
				if arg.endswith('.json'):
					exargs.append( arg )
			if 'qt' in sys.argv:
				log(__file__)
				if OSTYPE == 'Windows':
					cmd = ['py', __file__, '--qt'] + exargs
				else:
					cmd = ['python3', __file__, '--qt'] + exargs
				log(cmd)
				_qt_proc = subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
				threading._start_new_thread( update_qt_thread, tuple([]) )

			elif 'tk' in sys.argv:
				if OSTYPE == 'Windows':
					cmd = ['py', __file__, '--tk'] + exargs
				else:
					cmd = ['python', __file__, '--tk'] + exargs
				log(cmd)
				_qt_proc = subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
				threading._start_new_thread( update_qt_thread, tuple([]) )

			elif os.path.isfile('/usr/bin/chromium-browser'):
				os.system('/usr/bin/chromium-browser --app=%s/panjeaplugin_tool  --window-size=340,500 &' % url)
			else:
				webbrowser.open(url)

			bpy.ops.panjea.main()
			return {'FINISHED'}
		


	_is_recording = False
	@bpy.utils.register_class
	class PanjeaRecStart(bpy.types.Operator):
		"start recording"
		bl_idname = "panjea.rec_start"
		bl_label = "record"
		bl_options = {'REGISTER'}
		def invoke(self, context, event):
			global _is_recording
			_is_recording = True
			return {'FINISHED'}
		def execute(self, context):
			return self.invoke(context, None)


	@bpy.utils.register_class
	class PanjeaRecStop(bpy.types.Operator):
		"stop recording"
		bl_idname = "panjea.rec_stop"
		bl_label = "stop"
		bl_options = {'REGISTER'}
		def invoke(self, context, event):
			global _is_recording
			_is_recording = False
			context.scene.frame_end = context.scene.frame_current
			return {'FINISHED'}
		def execute(self, context):
			return self.invoke(context, None)


	if not BLENDER27:
		@bpy.utils.register_class
		class TOPBAR_HT_panjea(bpy.types.Header):
			bl_space_type = 'TOPBAR'
			def draw(self, context):
				region = context.region
				if region.alignment == 'RIGHT':
					layout = self.layout
					if 'qt' not in sys.argv:
						layout.operator("panjea.open")

	EngineReady = False
	STARTUP_SCRIPTS = []
	_mainloop_timer = None
	_last_rec_frame = -1
	_prev_msg_to_qt = ''
	_hotkeys = {'F1':None, 'F2':None, 'F3':None, 'F4':None, 'F5':None, 'F6':None, 'F7':None, 'F8':None, 'F9':None, 'F10':None, 'F11':None, 'F12':None}
	@bpy.utils.register_class
	class PanjeaMain(bpy.types.Operator):
		"panjea mainloop"
		bl_idname = "panjea.main"
		bl_label = "panjea main"
		bl_options = {'REGISTER'}

		def modal(self, context, event):
			global _last_rec_frame, STARTUP_SCRIPTS, EngineReady, _prev_msg_to_qt
			#print(event.type)
			if event.type == "TIMER":

				if _is_recording:
					_last_rec_frame = context.scene.frame_current
					context.scene.frame_current += 1
				do_http_updates()
				if api:
					api.update()

				if len(STARTUP_SCRIPTS):
					for arg in STARTUP_SCRIPTS:
						scope = globals()
						script = open(arg,'rb').read().decode('utf-8')
						try:
							exec(script, scope, scope )
						except:
							log('ERROR in script:', arg)
					STARTUP_SCRIPTS = []

				if _qt_proc and 'tk' not in sys.argv:
					qtmsg = {"windows":[{"x":win.x, "y":win.y, "w":win.width, "h":win.height} for win in bpy.context.window_manager.windows]}
					qtmsg = json.dumps(qtmsg)
					if qtmsg != _prev_msg_to_qt:
						_prev_msg_to_qt = qtmsg
						_qt_proc.stdin.write( qtmsg.encode('utf-8') + b'\n' )
						_qt_proc.stdin.flush()

			#elif event.type and event.type[0]=='F' and event.type in _hotkeys:
			#	## F1-F12 camera bookmarks ##
			#	if event.alt or event.ctrl or event.shift:
			#		_hotkeys[ event.type ] = None
			#	if 'Camera' in bpy.data.objects:
			#		if _hotkeys[ event.type ]:
			#			bpy.data.objects['Camera'].matrix_local = _hotkeys[event.type]
			#		else:
			#			_hotkeys[event.type] = bpy.data.objects['Camera'].matrix_local.copy()
			#	return {'RUNNING_MODAL'}
			elif api and api.use_keyboard_mouse:
				return api.process_event( event )
			return {'PASS_THROUGH'}  ## will not supress event bubbles

		def invoke(self, context, event):
			global _mainloop_timer
			if _mainloop_timer is None:
				do_on_load_scripts()
				_mainloop_timer = self._timer = context.window_manager.event_timer_add(
					time_step=0.0333,
					window=context.window
				)
				context.window_manager.modal_handler_add(self)
				return {'RUNNING_MODAL'}
			#else:
			#	context.window_manager.event_timer_remove(_mainloop_timer)
			#	_mainloop_timer = None
			return {'FINISHED'}

		def execute(self, context):
			return self.invoke(context, None)


	def update_qt_thread(*args):
		global __http_gpencil__, __ode_update__, Bands, __make_webcams__
		while _qt_proc:
			if _EXIT_:
				break
			#time.sleep(0.01)
			#else:
			#	update_pocket_pipe()
			#log('reading stdout...')
			pipe = _qt_proc.stdout.readline().decode('utf-8')
			#log('read OK')
			pipe = pipe.strip()
			#if len(pipe):
			#	print( len(pipe) )
			if pipe.startswith('[') and pipe.endswith(']'):
				try:
					msg = json.loads( pipe )
				except json.decoder.JSONDecodeError:
					log('ERROR: json.decoder.JSONDecodeError')
					log(pipe)
					break
				#print(len(msg))
				__http_gpencil__ = [ {"name":"WEBCAM", "layers": msg} ]
			elif pipe.startswith('{') and pipe.endswith('}'):
				try:
					msg = json.loads( pipe )
				except json.decoder.JSONDecodeError:
					log('ERROR: json.decoder.JSONDecodeError')
					log(pipe)
					break
				if 'module' in msg:
					exec(msg['module'], globals(), globals())
				elif 'ode' in msg:
					## this update is faster because all objects are sent in the same message
					__ode_update__ = msg['ode']
				elif 'webcam' in msg:
					__make_webcams__ = [ msg ]
				else:
					#log('============================')
					#log(msg)
					if 'cmd' in msg:
						__do_cmds__.append( msg )
					elif 'prim' in msg or 'inst' in msg:
						__make_prims__.append( msg )
					elif 'type' in msg:
						if msg['type'] == 'SCRIPT' and msg['name']:
							__make_txt__.append( msg )
					if api:
						if 'gamepad1' in msg:
							api.gamepad1 = msg['gamepad1']
						if 'gamepad2' in msg:
							api.gamepad2 = msg['gamepad2']
						if 'gamepad3' in msg:
							api.gamepad3 = msg['gamepad3']

					if 'bands' in msg:
						Bands = msg['bands']


			elif pipe.strip():
				if 'SDL_Joystick' in pipe:
					pass
				elif pipe.startswith('MSG:'):
					log(pipe)
				else:
					log("BAD MESSAGE")
					log(pipe)


	Volume = 0
	Speech = ''
	Gamepad = {}
	Bands = [0]*8


		
else:
	if OSTYPE=='Linux' and '--build' in sys.argv:
		build_audacity_fork()
		build_terminatorx_fork()

def main():
	if ISPLUGIN:
		## https://blender.stackexchange.com/questions/23126/is-there-a-way-to-execute-code-before-blender-is-closing
		atexit.register( exit_plugin )
		for arg in sys.argv[1:]:
			if arg.endswith('.py'):
				if not arg.endswith('panjeaplugin.py'):
					log('startup script:', arg)
					STARTUP_SCRIPTS.append(arg)
		if 'qt' in sys.argv or 'tk' in sys.argv:
			bpy.ops.panjea.open()
		try:
			#httpd = socketserver.TCPServer(('', 8081), Handler)  ## Python3
			httpd = SocketServer.TCPServer(("", 8081), Handler)
		except OSError:
			## [Errno 98] Address already in use
			httpd = None
		if httpd:
			threading._start_new_thread( httpd.serve_forever, tuple([]) )

		#bpy.ops.panjea.cell_fracture()

	elif '--qt' in sys.argv:
		assert Qt
		main_qt()
	elif '--tk' in sys.argv:
		assert HAS_TK
		main_tk()
	elif '--pipe' in sys.argv:
		main_subproc()
	else:
		if BLENDER_PATH:
			BLEND_FILE = ''
			USER_GRAPH = ''
			USER_SCRIPT = ''
			exargs = []
			colors = 64
			for arg in sys.argv[1:]:
				if arg.endswith('.blend'):
					BLEND_FILE = os.path.abspath( arg )
				elif arg.endswith('.py'):
					if not arg.endswith('panjeaplugin.py'):
						USER_SCRIPT = arg
						exargs.append( arg )
				elif arg.endswith('.json'):
					USER_GRAPH = arg
					exargs.append( arg )
				elif arg.startswith('--colors='):
					colors = int(arg.split('=')[-1])
					if colors > 256:
						raise RuntimeError('sixel max color is 256')

			cmd = []
			if OSTYPE=='Linux' and os.path.isfile('/usr/bin/xterm') and '--xterm' in sys.argv:
				#cmd.extend( 'xterm -ti vt340 -e'.split() )  ## only 16 colors by default
				cmd.extend(['xterm' ,'-xrm', "XTerm*decTerminalID: vt340", '-xrm', "XTerm*numColorRegisters: %s" %colors, '-e'])

			cmd.append( BLENDER_PATH )
			#cmd.append('--no-window-focus')


			if BLEND_FILE:
				cmd += [BLEND_FILE]
			if Qt:
				#cmd += ['--window-geometry', '64', '420', '720', '320', '--python', 'panjeaplugin.py', '--', 'qt']
				cmd += ['-noaudio', '--window-geometry', '100', '100', '400', '800', '--python', __file__, '--', 'qt']
				cmd += exargs
			elif HAS_TK:
				cmd += ['-noaudio', '--window-geometry', '64', '420', '720', '320', '--python', __file__, '--', 'tk']
				cmd += exargs
			else:
				cmd += ['-noaudio', '--python', __file__]
			log(cmd)
			subprocess.check_call(cmd)




def setup_engine(qapp):
	cache = os.path.expanduser('~/.panjea')
	if not os.path.isdir(cache):
		## plugins and models are downloaded here ##
		os.mkdir( cache )

	if '--offline' in sys.argv or FORCE_OFFLINE:
		pass
	else:
		if ISPY3:
			import urllib.request as urllib2
		else:
			import urllib2

		if ISPY3:
			ext = 'py'
		else:
			ext = 'pyc'

		Z = None
		if OSTYPE=="Windows":
			if os.path.isfile('C:/Program Files/7-Zip/7z.exe'):
				Z = 'C:/Program Files/7-Zip/7z.exe'
		elif os.path.isfile('/usr/bin/7zr'):
			Z = '/usr/bin/7zr'
		if Z:
			ext = '7z'

		if ISPY3:
			url = 'http://www.panjea.com/pyjea.py3_%s' %ext
			if OSTYPE=='Linux' and USERNAME=='raptor':
				url = 'http://localhost:8080/pyjea.py3_%s' %ext
		else:
			url = 'http://www.panjea.com/pyjea._%s' %ext
			if OSTYPE=='Linux' and USERNAME=='raptor':
				url = 'http://localhost:8080/pyjea._%s' %ext

		log('downloading:', url)
		try:
			data = urllib2.urlopen(url, timeout=3).read()
			open( os.path.join(THISDIR,'pyjea.%s' %ext), 'wb').write( data )
		except:
			log('failed to download:', url)

		if Z and os.path.isfile(os.path.join(THISDIR,'pyjea.%s' %ext)):
			log('extracting pyjea.7z')
			try:
				subprocess.check_call([Z, 'e', '-y', 'pyjea.%s'%ext], cwd=THISDIR)
			except:
				log('failed to extract pyjea.7z')
				try:
					os.unlink(os.path.join(THISDIR,'pyjea.%s' %ext))
				except:
					pass

		if not os.path.isfile( os.path.join(THISDIR,'pyjea.zip') ) or ISPY3:
			if ISPY3:
				url = 'http://www.panjea.com/pyjea.py3_zip'
				if OSTYPE=='Linux' and USERNAME=='raptor':
					url = 'http://localhost:8080/pyjea.py3_zip'
			else:
				url = 'http://www.panjea.com/pyjea.zip'
				if OSTYPE=='Linux' and USERNAME=='raptor':
					url = 'http://localhost:8080/pyjea.zip'
			log('downloading:', url)
			try:
				data = urllib2.urlopen(url, timeout=3).read()
				open( os.path.join(THISDIR,'pyjea.zip'), 'wb').write(data)
			except:
				log('failed to download:', url)
	
	sys.path.append( THISDIR )
	import pyjea
	pyjea.API['set_qapp'](qapp)
	pyjea.API['set_user_name'](USERNAME)
	pyjea.API['set_ffmpeg_path']( FFMPEG )
	pyjea.API['init_engine']()
	pyjea.API['init_plugins']()
	return pyjea

_QT_WEBCAM = None

if OSTYPE=='Linux':
	FONT = 'Noto Color Emoji'
elif OSTYPE=='Windows':
	FONT = 'Segoe UI Emoji'
else:
	FONT = 'Apple Color Emoji'

def main_tk():
	pyjea = setup_engine()

	def read_from_clipboard():
		if OSTYPE == "Linux":
			return subprocess.check_output(['xclip','-o']).decode('utf-8')
		else:
			return subprocess.check_output('pbpaste', env={'LANG': 'en_US.UTF-8'}).decode('utf-8')

	def export_blender(evt):
		clipboard = read_from_clipboard()
		msg = {'type':"SCRIPT", 'name':'unnamed.tile', 'script':clipboard}
		print( json.dumps(msg) )
		sys.stdout.flush()

	def export_panjea(evt):
		msg = {'cmd':"EXPORT"}
		print( json.dumps(msg) )
		sys.stdout.flush()

	root = Tk()
	root.title("PanjeaPlugin")
	root.geometry("420x160")
	frame = Frame(root)
	frame.pack()

	w = Label(frame, text="Panjea Plugin for OSX")
	w.pack()

	w = Label(frame, text="copy your map, then click here:")
	w.pack(side=LEFT)

	w = Button(frame, text = "export to Blender")
	w.pack(side=LEFT)
	w.bind("<ButtonPress-1>", export_blender)

	frame = Frame(root)
	frame.pack()

	w = Label(frame, text="THREE.js preview: ")
	w.pack(side=LEFT)

	w = Button(frame, text = "export to Panjea.com")
	w.pack(side=LEFT)
	w.bind("<ButtonPress-1>", export_panjea)

	frame = Frame(root)
	frame.pack(side=BOTTOM)

	w = Label(frame, text="-"*160)
	w.pack()

	w = Label(frame, text="this interface is a fallback, because your missing PyQt5 for Python2")
	w.pack()
	w = Label(frame, text="to install PyQt5 for Python2, try this command:")
	w.pack()
	w = Label(frame, text="sudo pip install python-qt5")
	w.pack()

	root.mainloop()

def main_qt():
	log(Qt)
	app = QApplication(['--enable-logging'])
	log(app)
	pyjea = setup_engine(app)
	pyjea.API['main_loop']( app )






if __name__ == '__main__':
	main()

