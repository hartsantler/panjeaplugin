#!/bin/bash
cd

sudo dnf -y install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
sudo dnf -y install https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
sudo dnf -y install https://github.com/rpmsphere/noarch/raw/master/r/rpmsphere-release-32-1.noarch.rpm


sudo dnf install git cmake meson ninja-build gtk3 python3-gobject python3-cairo python3-numpy python3-scipy gstreamer1 gstreamer1-plugins-good gstreamer1-plugins-bad-free gstreamer1-plugins-bad-free-devel python3-gstreamer1 ladspa-swh-plugins ladspa-calf-plugins boost-devel itstool agg-devel g++ pulseaudio-libs-devel gtkmm30-devel gstreamer1-devel lilv-devel libsndfile-devel libbs2b-devel zita-convolver-devel libebur128-devel

## https://bugzilla.redhat.com/show_bug.cgi?id=1819739
sudo dnf install libnice-gstreamer1 gstreamer1-plugins-ugly gstreamer1-plugins-good gstreamer gstreamer1 gstreamer1-plugins-base-tools gstreamer1-plugins-bad-free-extras gstreamer1-plugins-bad-freeworld gstreamer1-plugins-base gstreamer1-plugins-good-extras gstreamer-tools PackageKit-gstreamer-plugin gstreamer1-plugins-bad-free lsp-plugins-lv2

## https://github.com/wwmm/pulseeffects/issues/583
sudo dnf install lv2-mdala-plugins lv2 calf fluidsynth-libs game-music-emu lash-libs libchromaprint libkate libofa lv2-calf-plugins zita-convolver rubberband lv2-zam-plugins zam-plugins

cd
git clone https://gitlab.com/hartsantler/pulseeffects.git
cd pulseeffects/
./build.sh
cd


#python2 python-ply python-pycparser

sudo dnf install libevent-devel zip espeak vorbis-tools p7zip python3-qt5 python3-qt5-webengine python3-opencv python3-pyaudio python3-pygame python3-nose python3-pillow python3-Cython ffmpeg gtk3-devel v4l-utils libv4l-devel xterm potrace-devel pavucontrol nodejs hydrogen

sudo dnf install qt5-qtbase-devel qt5-qtx11extras-devel qt5-qtxmlpatterns-devel qt5-qttranslations qt5-qttools-devel qt5-qtsvg-devel qt5-qtdeclarative-devel libarchive-devel alsa-lib-devel libtar-devel ladspa-devel
cd
git clone https://gitlab.com/hartsantler/pandrogen.git
cd pandrogen
mkdir build
cd build
cmake ..
make
sudo make install

cd
git clone https://github.com/atareao/python3-v4l2capture
cd python3-v4l2capture/
python3 setup.py build
sudo python3 setup.py install

cd
git clone https://gitlab.com/hartsantler/dlib.git
cd dlib
python3 setup.py build
sudo python3 setup.py install

cd
git clone https://gitlab.com/hartsantler/pypotrace.git
cd pypotrace
python3 setup.py build
sudo python3 setup.py install

