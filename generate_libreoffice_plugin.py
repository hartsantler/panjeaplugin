#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import os, sys, subprocess

PYRILLIC = u'➕➖❌➗λ🎲🂿'

PLUGINS = u'𓁆♜⚰𓆏𓅹🏠🌲🌳𐇲𔘒𔒲𔔎🏕👰🐴🏗🕱'

PYTILES = u'～░▦▩🛣𓈖𓈗＿－￣⺾⺿⻀⾋𔓐𓊗🝙🝚𔓒⽯𐇶𓇔𓈉𓈋𓇹𔔉＊𓄧●⬤▲▯⌷⭖⭗⽳'

HEIGHT_SYMS = u'①②③④⑤⑥⑦⑧⑨⑩⑪⑫⑬⑭⑮⑯⑰⑱⑲⑳'
BLOCK_SYMS  = u'█▇▆▅▄▃▂▁'
PLATFORM_SYMS = u'⑴⑵⑶⑷⑸⑹⑺⑻⑼⑽⑾⑿⒀⒁⒂⒃⒄⒅⒆⒇'
CRACKED_PLATFORM_SYMS = u'⒈⒉⒊⒋⒌⒍⒎⒏⒐⒑⒒⒓⒔⒕⒖⒗⒘⒙⒚⒛'
PYRILLIC_CLASS_SYMS = u'бвгджзклмнопсхцчш'

PYRILLIC_INST_SYMS = u''
PYRILLIC_INST_SYMS += u' ⷠ '.strip()
PYRILLIC_INST_SYMS += u' ⷡ '.strip()
PYRILLIC_INST_SYMS += u' ⷢ '.strip()
PYRILLIC_INST_SYMS += u' ⷣ '.strip()
PYRILLIC_INST_SYMS += u' ⷤ '.strip()
PYRILLIC_INST_SYMS += u' ⷥ '.strip()
PYRILLIC_INST_SYMS += u' ⷦ '.strip()
PYRILLIC_INST_SYMS += u' ⷧ '.strip()
PYRILLIC_INST_SYMS += u' ⷨ '.strip()
PYRILLIC_INST_SYMS += u' ⷩ '.strip()
PYRILLIC_INST_SYMS += u' ⷪ '.strip()
PYRILLIC_INST_SYMS += u' ⷫ '.strip()
PYRILLIC_INST_SYMS += u' ⷭ '.strip()
PYRILLIC_INST_SYMS += u' ⷯ '.strip()
PYRILLIC_INST_SYMS += u' ⷰ '.strip()
PYRILLIC_INST_SYMS += u' ⷱ '.strip()
PYRILLIC_INST_SYMS += u' ⷲ '.strip()


TOOLBARS = {
	'plugins'	: PLUGINS,
	'tiles'		: PYTILES,
	'heights'   : HEIGHT_SYMS,
	'blocks'    : BLOCK_SYMS,
	'platforms' : PLATFORM_SYMS,
	'cracked_platforms' : CRACKED_PLATFORM_SYMS,
	'classes'   : PYRILLIC_CLASS_SYMS,
	'instances' : PYRILLIC_INST_SYMS,
	'syntax'    : PYRILLIC,
}



##https://wiki.openoffice.org/wiki/Documentation/DevGuide/WritingUNO/AddOns/Toolbars

Addons_xcu = '''<?xml version="1.0" encoding="UTF-8"?>
<oor:component-data xmlns:oor="http://openoffice.org/2001/registry" 
 xmlns:xs="http://www.w3.org/2001/XMLSchema" 
 oor:name="Addons" oor:package="org.openoffice.Office">
   <node oor:name="AddonUI">
%s
   </node>
</oor:component-data>
'''


PanjeaSymbols_xba = '''<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE script:module PUBLIC "-//OpenOffice.org//DTD OfficeDocument 1.0//EN" "module.dtd">
<script:module xmlns:script="http://openoffice.org/2000/script" script:name="PanjeaSymbols" script:language="StarBasic">

%s

Sub InsertSymbol(Character As String)
    Dim oDoc As Object
    Dim oDispatcher As Object
    oDoc = ThisComponent.CurrentController.Frame
    oDispatcher = createUnoService("com.sun.star.frame.DispatchHelper")
    Dim args1(0) as new com.sun.star.beans.PropertyValue
    args1(0).Name = "Symbols"
    args1(0).Value = Character
    oDispatcher.executeDispatch(oDoc, ".uno:InsertSymbol", "", 0, args1())
End Sub
</script:module>
'''

script_xlb = '''<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE library:library PUBLIC "-//OpenOffice.org//DTD OfficeDocument 1.0//EN" "library.dtd">
<library:library xmlns:library="http://openoffice.org/2000/library" library:name="Panjea" library:readonly="false" library:passwordprotected="false">
	<library:element library:name="PanjeaSymbols"/>
</library:library>
'''

dialog_xlb = '''<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE library:library PUBLIC "-//OpenOffice.org//DTD OfficeDocument 1.0//EN" "library.dtd">
<library:library xmlns:library="http://openoffice.org/2000/library" library:name="Panjea" library:readonly="false" library:passwordprotected="false"/>
'''

description_xml = '''<?xml version='1.0' encoding='UTF-8'?>
<description
 xmlns="http://openoffice.org/extensions/description/2006"
 xmlns:dep="http://openoffice.org/extensions/description/2006"
 xmlns:xlink="http://www.w3.org/1999/xlink">
	<identifier value="org.panjea.hartsantler"/>
	<icon>
        <default xlink:href="icons/panjea42.png" />
    </icon>
	<version value="1.1"/>
	<registration>
		<simple-license  accept-by="admin" default-license-id="ID0" suppress-on-update="true" >
			<license-text xlink:href="registration/license.txt" lang="en" license-id="ID0" />
		</simple-license>
	</registration>
     <publisher>
	      <name xlink:href="https://www.panjea.com">Panjea</name>
      </publisher>
       <display-name>
            <name>Panjea Symbols</name>
       </display-name>
</description>'''

manifest_xml = '''<?xml version="1.0" encoding="UTF-8"?>
<manifest:manifest>
 <manifest:file-entry manifest:full-path="Panjea/" manifest:media-type="application/vnd.sun.star.basic-library"/>
 <manifest:file-entry manifest:full-path="pkg-desc/pkg-description.en" manifest:media-type="application/vnd.sun.star.package-bundle-description;locale=en"/>
 <manifest:file-entry manifest:full-path="Addons.xcu" manifest:media-type="application/vnd.sun.star.configuration-data"/>
</manifest:manifest>'''

def generate(path='/tmp/panjea_oxt'):
	os.system( 'rm -rf ' + path)
	if not os.path.isdir(path):
		os.mkdir(path)
		os.mkdir( os.path.join(path, 'Panjea') )
		os.mkdir( os.path.join(path, 'META-INF') )
		os.mkdir( os.path.join(path, 'pkg-desc') )
		os.mkdir( os.path.join(path, 'icons') )
		os.mkdir( os.path.join(path, 'registration') )

	open( os.path.join(path, 'description.xml'), 'wb').write( description_xml )
	open( os.path.join(path, 'META-INF/manifest.xml') ,'wb').write( manifest_xml )
	open( os.path.join(path, 'pkg-desc/pkg-description.en') ,'wb').write( "Panjea Symbols extension. Copyright (c) Hartsantler and Panjea.com" )
	open( os.path.join(path, 'registration/license.txt') ,'wb').write( "GNU LGPL" )

	os.system('cp -v ./icons/panjea42.png %s/icons/.' %path)

	##chararray = [] ## Inadmissable value or data type overflow
	funcs = []
	nodes = []

	idx = 0
	for tbname in TOOLBARS:
		symbols = TOOLBARS[tbname]
		nodes.append('<node oor:name="OfficeToolBar"><node oor:name="org.panjea.hartsantler.%s" oor:op="replace">' %tbname)

		for symbol in symbols:
			symbol = symbol.strip()
			if not symbol:
				raise RuntimeError('blank symbol')
			assert len(symbol)==1
			#chararray.append( 'Chr(%s)' % ord(symbol) )  ## Chr can not take a long number

			func = [
				'Sub sym%s' %idx,
				'    InsertSymbol("%s")' %symbol,
				'End Sub'
			]
			funcs.extend( func )

			node = [
				'<node oor:name="sym%s" oor:op="replace">' %idx ,
				'	<prop oor:name="Context" oor:type="xs:string">',
				'		<value>com.sun.star.text.TextDocument</value>',
				'	</prop>',
				'	<prop oor:name="Title" oor:type="xs:string">',
				'		<value xml:lang="en">%s</value>' %symbol,
				'	</prop>',
				'	<prop oor:name="URL" oor:type="xs:string">',
				'		<value>vnd.sun.star.script:Panjea.PanjeaSymbols.sym%s?language=Basic&amp;location=application</value>' %idx,
				'	</prop>',
				'		<prop oor:name="Target" oor:type="xs:string">',
				'		<value>_self</value>',
				'	</prop>',
				'</node>'
			]
			nodes.extend(node)
			idx += 1

		nodes.append('</node></node>')

	open(os.path.join(path, 'Panjea/script.xlb'), 'wb').write( script_xlb )
	open(os.path.join(path, 'Panjea/dialog.xlb'), 'wb').write( dialog_xlb )

	#data = PanjeaSymbols_xba % ('\n'.join(funcs), len(chararray), ','.join(chararray))
	data = PanjeaSymbols_xba % '\n'.join(funcs)
	open(os.path.join(path, 'Panjea/PanjeaSymbols.xba'), 'wb').write( data.encode('utf-8') )

	data = Addons_xcu % '\n'.join(nodes)
	open(os.path.join(path, 'Addons.xcu'), 'wb').write( data.encode('utf-8') )

	subprocess.check_call('zip -r panjea_libreoffice.oxt *', shell=True, cwd=path)


generate()
