#!/bin/bash
cd
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"

brew update
brew install python3
brew install qt5
brew install sip
brew install pyqt5