@ECHO OFF

curl.exe -C - --output python39.exe --url https://www.python.org/ftp/python/3.9.1/python-3.9.1-amd64.exe
python39.exe
py -m pip install PyQt5
py -m pip install PyQt5-sip
py -m pip install PyQtWebEngine
py -m pip install numpy
py -m pip install scipy
py -m pip install opencv-python
py -m pip install pygame
py -m pip install pyaudio

ECHO "install complete"
PAUSE
