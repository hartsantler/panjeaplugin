# PanjeaPlugin
python logic nodes and bricks, audio synth nodes, and tile mapping language for blender

https://www.panjea.com/panjeaplugin

https://www.panjea.com/blockjia

https://www.panjea.com/pyjea

https://www.panjea.com/pyrillic

https://www.panjea.com/synth

# Windows10
```
install-windows.bat
panjeaplugin.bat
```
Note that this will download and install Python3.9, pip, and Blender if not found.
Make sure you install Python and Blender to their default paths on your C: drive.

If you have installed Blender to a non default path,
then use panjeaplugin-custom-blender.bat,
edit the file and change `--blender=C:\path\to\blender.exe` to point to where you have blender installed

# Aegean and Anatolian Fonts

On Windows you will need to install two fonts: Aegean.ttf and Anatolian.ttf
Double click it to install them.

# 7zip

Install 7zip to the default path to speed up downloads from our server.

https://www.7-zip.org/download.html

# Linux
```
cd panjeaplugin
./panjeaplugin.py
```

# Installing Ubuntu20
```
cd panjeaplugin
./install-ubuntu.sh
```

# Installing Fedora33
```
cd panjeaplugin
./install-fedora.sh
```


# Mac OSX
```
cd panjeaplugin
./panjeaplugin.py
```
Note on OSX: PyQt5 will NOT be automatically installed, and will fallback to using a minimal TK interface.
OSX is not a supported platform.

# Tested
* Windows10
* Ubuntu 16.04
* Ubuntu 18.04
* Ubuntu 19.10
* Ubuntu 20.04
* Fedora 28
* Fedora 33

# Requirements
* Blender 2.79 or Blender 2.8 or Blender2.9
* Python 3
* PyQt5 5.5.1 or newer
* Numpy
* Scipy
* PyAudio
* FFMPEG

# Optional
* Dlib
* OpenCV
* PyPotrace
* PyODE

# Hardware
* Microphone
* Webcamera
* Gamepads
* Multi-Touch Screens

# LibreOffice Extension

https://panjeastatic.s3.us-east-2.amazonaws.com/releases/panjea_libreoffice.oxt


# MS Word Add-in

To install the MS Word Add-in you need to "side load" it.
For Desktop MS Word, share this folder as a network share, copy its network share path,
go to MS Office settings, trusted catalogs, select the network share,
then enable the Panjea Plugin.

https://docs.microsoft.com/en-us/office/dev/add-ins/testing/create-a-network-shared-folder-catalog-for-task-pane-and-content-add-ins

https://docs.microsoft.com/en-us/office/dev/add-ins/testing/sideload-office-add-ins-for-testing#sideload-an-office-add-in-in-office-on-the-web

